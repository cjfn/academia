$(document).ready(function() {
    var URLdomain = window.location.host;//para obtener el host del sitio
    var protocolo = window.location.protocol;//para obtenerl el protocolo del sitio

    //var URLdomain += '/education/public';
    //var url = protocolo+'//'+URLdomain+'/municipio';
    $('#datosPlanes').on('click', '.eliminarPlan', function(event) {//evento on click para los elemtos que tengan la clase eliminarPlan en el index plan
      event.preventDefault();
      var id = $(this).data('id');//obtener el id del plan
      var estado = $(this).data('estado');//obtener el estado actual del plan
      var url = protocolo+'//'+URLdomain+'/planes/'+id;//la url a donde se hara la peticion para eliminar
      var token = $('#token').val();//obtenmos el token para enviar la peticion ajax
      $('#miModal').css('opacity', '1');
      $('#miModal').css('pointer-events', 'auto');
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',//se indica el tipo de peticion
        dataType: 'JSON',//el tipo de dato
        data: {estado: estado}//los datos
      }).error(function(error) {//cuando ocurra un error en la peticion ajax
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
        console.log(error);
        console.log('error');//escribimos erro cuando ocurra un error en la peticion
      }).success(function(data) {//cuando la peticion ajax fue exitosa
        var html = '';
        $(data).each(function(index, el) {//recorremos todos los datos recibidos de la la peticion
          html += '<tr>';//creamos el html que se incrustrar en la pagina
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_plan.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/planes/'+el.id_plan+'/edit" class="btn btn-primary">Editar</a></td>'
          if (el.estado_plan == true) {//cuando el estado sea verdadero
              html += '<td><a href="#" data-id="'+el.id_plan+'" data-estado="'+el.estado_plan+'" class="btn btn-success eliminarPlan">Habilitado</a></td>';
          }else {//cuando el estado sea falso
            html += '<td><a href="#" data-id="'+el.id_plan+'" data-estado="'+el.estado_plan+'" class="btn btn-danger eliminarPlan">Deshabilitado</a></td>';
          }//fin del if else
          html += '</tr>';
        });
        $('#datosPlanes').html(html);//incrustamos el html en la etiqueta donde tenga el id datosPlanes
        console.log('listo');//escribimos en la consolaun mensaje de listo
        //ocultar el modal despues de 1 segundo
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
        //fin de ocultar el modal
      });//fin del evento success
    });//fin del evento on click

    $('#datosJornadas').on('click', '.eliminarJornada', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/jornadas/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {id: id, estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
            html += '<tr>';
            html += '<td>'+(index+1)+'</td>';
            html += '<td>'+el.nombre_jornada.toUpperCase()+'</td>';
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/jornadas/'+el.id_jornada+'/edit'+'" class="btn btn-primary">Editar</a></td>';
            if (el.estado_jornada == true) {
              html += '<td><a href="'+protocolo+'//'+URLdomain+'/jornadas/'+'" class="btn btn-success eliminarJornada" data-id="'+el.id_jornada+'" data-estado="'+el.estado_jornada+'">Habilitado</a></td>';
            }else {
              html += '<td><a href="'+protocolo+'//'+URLdomain+'/jornadas/'+'" class="btn btn-danger eliminarJornada" data-id="'+el.id_jornada+'" data-estado="'+el.estado_jornada+'">Deshabilitado</a></td>';
            }
            html += '</tr>';
        });
        $('#datosJornadas').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      });//fin del evento ajax

    });//fin del vento on click

    $('#datosNiveles').on('click', '.eliminarNivel', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/niveles/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_nivel.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/niveles/'+el.id_nivel+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_nivel == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/niveles/'+'" class="btn btn-success eliminarNivel" data-id="'+el.id_nivel+'" data-estado="'+el.estado_nivel+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/niveles/'+'" class="btn btn-danger eliminarNivel" data-id="'+el.id_nivel+'" data-estado="'+el.estado_nivel+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosNiveles').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
      });//fin de la peticion ajax

    });//fin del evento on click

    $('#datosPuestos').on('click', '.eliminarPuesto', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/puestos/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_puesto.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/puestos/'+el.id_puesto+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_puesto == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/puestos/'+'" class="btn btn-success eliminarPuesto" data-id="'+el.id_puesto+'" data-estado="'+el.estado_puesto+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/puestos/'+'" class="btn btn-danger eliminarPuesto" data-id="'+el.id_puesto+'" data-estado="'+el.estado_puesto+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosPuestos').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#datosGrados').on('click', '.eliminarGrado', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/grados/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_grado.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/grados/'+el.id_grado+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_grado == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/grados/'+'" class="btn btn-success eliminarGrado" data-id="'+el.id_grado+'" data-estado="'+el.estado_grado+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/grados/'+'" class="btn btn-danger eliminarGrado" data-id="'+el.id_grado+'" data-estado="'+el.estado_grado+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosGrados').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#datosSalones').on('click', '.eliminarSalon', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/salones/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_salon.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/salones/'+el.id_salon+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_salon == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/salones/'+'" class="btn btn-success eliminarSalon" data-id="'+el.id_salon+'" data-estado="'+el.estado_salon+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/salones/'+'" class="btn btn-danger eliminarSalon" data-id="'+el.id_salon+'" data-estado="'+el.estado_salon+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosSalones').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#datosAreas').on('click', '.eliminarArea', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/areas/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_area.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/areas/'+el.id_area+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_area == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/areas/'+'" class="btn btn-success eliminarArea" data-id="'+el.id_area+'" data-estado="'+el.estado_area+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/areas/'+'" class="btn btn-danger eliminarArea" data-id="'+el.id_area+'" data-estado="'+el.estado_area+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosAreas').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        console.log("error");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#datosCarreras').on('click', '.eliminarCarrera', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/carreras/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_carrera.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/carreras/'+el.id_area+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_carrera == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/carreras/'+'" class="btn btn-success eliminarCarrera" data-id="'+el.id_carrera+'" data-estado="'+el.estado_carrera+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/carreras/'+'" class="btn btn-danger eliminarCarrera" data-id="'+el.id_carrera+'" data-estado="'+el.estado_carrera+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosCarreras').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        alert('Lo sentimos parece que a ocurrido un error al procesar la solicitud en el servidor, vuelva intentarlo más tarde!!!');
        console.log("error");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#jngradonivel').change(function(event) {
      event.preventDefault();
      var plan = $('#pngradonivel').val();
      var jornada = $('#jngradonivel').val();
      if (plan == '' || jornada == '') {
        alert('Es necesario que seleccione una jornada y un plan');
      }else {
        var url = protocolo+'//'+URLdomain+'/asignarniveles/obtenerajx';
        $('#miModal').css('opacity', '1');//activar la carga modal
        $('#miModal').css('pointer-events', 'auto');//activar la carga modal
        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          data: {jornada: jornada, plan: plan}
        })
        .success(function(data) {
          var html = '<option value="">Seleccione un nivel...</option>';
          $(data).each(function(index, el) {
            html += '<option value="'+el.id_nivel_plan_jornada+'">'+el.nombre_nivel.toUpperCase()+'</option>';
          });
          $('#nngradonivel').html(html);
          console.log("success");
          setTimeout(function(){//ocultar el modal
          $('#miModal').css('opacity', '0');
          $('#miModal').css('pointer-events', 'none');},
          1000);
        })
        .error(function() {
          alert('Lo sentimos parece que a ocurrido un error al procesar la solicitud en el servidor, vuelva intentarlo más tarde!!!');
          console.log("error");
          setTimeout(function(){//ocultar el modal
          $('#miModal').css('opacity', '0');
          $('#miModal').css('pointer-events', 'none');},
          1000);
        });

      }
    });

    $('#datosGradosNiveles').on('click', '.eliminarGradoNivel', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var estado = $(this).data('estado');
      var token = $('#token').val();
      var url = protocolo+'//'+URLdomain+'/gradoniveles/'+id;
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        data: {estado: estado}
      })
      .success(function(data) {
        var html = '';
        $(data).each(function(index, el) {
          html += '<tr>';
          html += '<td>'+(index+1)+'</td>';
          html += '<td>'+el.nombre_seccion.toUpperCase()+'</td>';
          html += '<td>'+el.nombre_grado.toUpperCase()+'</td>';
          html += '<td>'+el.nombre_nivel.toUpperCase()+'</td>';
          html += '<td>'+el.nombre_carrera.toUpperCase()+'</td>';
          html += '<td><a href="'+protocolo+'//'+URLdomain+'/gradoniveles/'+el.id_nivel_grado+'/edit'+'" class="btn btn-primary">Editar</a></td>';
          if (el.estado_nivel_grado == true) {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/gradoniveles/'+'" class="btn btn-success eliminarGradoNivel" data-id="'+el.id_nivel_grado+'" data-estado="'+el.estado_nivel_grado+'">Habilitado</a></td>';
          }else {
            html += '<td><a href="'+protocolo+'//'+URLdomain+'/gradoniveles/'+'" class="btn btn-danger eliminarGradoNivel" data-id="'+el.id_nivel_grado+'" data-estado="'+el.estado_nivel_grado+'">Deshabilitado</a></td>';
          }
          html += '</tr>';
        });
        $('#datosGradosNiveles').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function() {
        alert('Lo sentimos parece que a ocurrido un error al procesar la solicitud en el servidor, vuelva intentarlo más tarde!!!');
        console.log("error");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      });//fin de la peticion ajax
    });//fin del evento on click

    $('#jnpensum').change(function(event) {
      event.preventDefault();
      var plan = $('#pnpensum').val();
      var jornada = $(this).val();
      if (plan == '' || jornada == '') {
        alert('Es necesario que seleccione un plan y un nivel validos!!!');
      } else {
        $('#miModal').css('opacity', '1');//activar la carga modal
        $('#miModal').css('pointer-events', 'auto');//activar la carga modal
        var url = protocolo+'//'+URLdomain+'/pensum/grados';
        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          data: {plan: plan, jornada: jornada}
        })
        .success(function(data) {
          var html = '';
          $(data).each(function(index, el) {
            html += '<tr>';
            html += '<td>'+(index+1)+'</td>';
            html += '<td>'+el.nombre_grado.toUpperCase()+'</td>';
            html += '<td>'+el.nombre_nivel.toUpperCase()+'</td>';
            html += '<td>'+el.nombre_carrera.toUpperCase()+'</td>';
            html += '<td>'+el.nombre_seccion.toUpperCase()+'</td>';
            html += '<td><input type="checkbox" name="grado[]" value="'+el.id_nivel_grado+'"></td>';
            html += '</tr>';
          });
          $('#grados').html(html);
          console.log("success");
          //ocultar la carga modal
          setTimeout(function(){
          $('#miModal').css('opacity', '0');
          $('#miModal').css('pointer-events', 'none');},
          1000);
        })
        .error(function() {
            alert('Lo sentimos parece que a ocurrido un error al procesar la solicitud en el servidor, vuelva intentarlo más tarde!!!');
            console.log("error");
            setTimeout(function(){
            $('#miModal').css('opacity', '0');
            $('#miModal').css('pointer-events', 'none');},
            1000);
        });

      }
    });

    $('#jnGrado').change(function(event) {
      /* Act on the event */
      var plan = $('#pnGrado').val();
      var jornada = $('jnGrado').val();
      var url = protocolo+'//'+URLdomain+'/niveles/ajxniveles';
      $('#miModal').css('opacity', '1');//activar la carga modal
      $('#miModal').css('pointer-events', 'auto');//activar la carga modal
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {plan: plan, jornada: jornada}
      })
      .success(function(data) {
        var html = '';
        html += '<option value="">Seleccione nivel...</option>';
        $(data).each(function(index, el) {
          html += '<option value="'+el.id_nivel+'">'+el.nombre_nivel.toUpperCase()+'</option>';
        });
        $('#nnGrado').html(html);
        console.log("success");
        setTimeout(function(){
        $('#miModal').css('opacity', '0');
        $('#miModal').css('pointer-events', 'none');},
        1000);
      })
      .error(function(error) {
        console.log("error");
      });

    });//fin del evento change

    $('#agregarCurso').click(function(event) {
      event.preventDefault();
      var curso = $('#autocomplete').val();
      var id = $('#autocomplete').data('id');
      $('#autocomplete').data('id', '');
      if (id == '') {
        alert('Curso no valido, no esta registrado en la base de datos!!!');
      } else {
        var salon = $('#salon').val();

        if (salon == '') {
          alert('No ha selecionado salón, seleccione uno!!!');
        } else {
          var b = false;
          //recorremos todos los inputs existentes con .each
          $('input[name="cursos[]"]').each(function() {
            //$(this).val() es el valor del input correspondiente
            console.log($(this).val());
            if ($(this).val() == id) {
              b = true;
            }
          });
          if (b == false) {
            var html = '<div class="alert alert-success alert-dismissible" role="alert">';
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            html += '<strong>'+curso+'</strong><input type="hidden" name="cursos[]" value="'+id+'"><input type="hidden" name="salones[]" value="'+salon+'">';
            html += '</div>';
            $('#cursosPensum').append(html);
          }//fin del if
          $('#autocomplete').val('');
        }//fin del if else
      }//fin del if else

    });

    $('#borrarBuscar').click(function(event) {
      event.preventDefault();
      $('#autocomplete').val('');
      $('#autocomplete').data('id', '');
    });


      $('#datosPensum').on('change', '.toggleEstado', function(event) {
        event.preventDefault();
        var id = $(this).data('id');
        var estado = $(this).data('estado');
        var check = $(this).prop('checked');
        var token = $('#token').val();
        if (check == true) {
            estado = 0;
        }else {
          estado = 1;
        }
        var url = protocolo+'//'+URLdomain+'/pensum/'+id;
        $.ajax({
          url: url,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {estado: estado}
        })
        .success(function() {
          /*if (check == true) {
            $(this).attr("data-estado", "100");
          } else {
            $(this).attr("data-estado", "50");
          }*/
          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la peticion ajax

      });//fin del evento on change

      $('#agregarNuevoCurso').click(function(event) {
        event.preventDefault();
        jsonCursos = [];
        jsonSalones = [];
        //json de los curso asignados
        $('input[name="cursos[]"]').each(function(index, el) {
            console.log($(this).val());
            item = {}
            item ["curso"] = $(this).val();
            jsonCursos.push(item);
        });
        //json de los salones asignados a los cursos
        $('input[name="salones[]"]').each(function(index, el) {
            console.log($(this).val());
            item = {}
            item ["salon"] = $(this).val();
            jsonSalones.push(item);
        });
        //convertir a string json los jsons
        var cursos = JSON.stringify(jsonCursos);
        var salones = JSON.stringify(jsonSalones);
        var id = $('#nivelGrado').val();
        var token = $('#token').val();
        var url = protocolo+'//'+URLdomain+'/pensum/'+id;
        $.ajax({
          url: url,
          type: 'PUT',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {salon: salones, curso: cursos}
        })
        .success(function(data) {
          var html = '';
          $(data).each(function(index, el) {
              html += '<tr>';
              html += '<td>'+(index+1)+'</td>';
              html += '<td>'+el.nombre_area.toUpperCase()+'</td>';
              html += '<td>'+el.nombre_salon.toUpperCase()+'</td>';
              if (el.estado_asignacion_area == true) {
                html += '<td><input type="checkbox" name="_estado" value="1" checked="TRUE" class="toggleEstado" data-id="'+el.id_asignacion_area+'" data-estado="'+el.estado_asignacion_area+'"></td>';
              } else {
                html += '<td><input type="checkbox" name="_estado" value="0" class="toggleEstado" data-id="'+el.id_asignacion_area+'" data-estado="'+el.estado_asignacion_area+'"></td>';
              }
              html +='</tr>';
          });
          $('#datosPensum').html(html);
          $('#formModal').modal('hide');
          $('#cursosPensum').html('');
          /*agregar un toggle al checkbox que tenga la clase toggleEstado*/
          $('.toggleEstado').bootstrapToggle({
            on: 'Habilitado',
            off: 'Deshabilitado',
            onstyle: 'success',
            offstyle: 'danger'
          });//fin del toggle

          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la peticion ajax

      });//fin del evento guardar

      $('#opcionesSistema').change(function(event) {
        event.preventDefault();
        var opcion = $(this).val();
        //var url = protocolo+'//'+URLdomain+'/subopciones';
        var url = protocolo+'//'+URLdomain+'/subopciones';
        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          data: {opcion: opcion}
        })
        .success(function(data) {
          //console.log(data);
          var html = '';
          $(data).each(function(index, el) {
              html += '<tr>';
              html += '<td>'+(index+1)+'</td>';
              html += '<td>'+el.display_name.toUpperCase()+'</td>';
              html += '<td><button data-toggle="tooltip" data-placement="top" title="'+el.description+'" type="button" name="agregar" class="btn btn-info agregarPermiso" data-nombre="'+el.display_name.toUpperCase()+'" data-id="'+el.id+'"><span class="fa fa-check-circle"></span> Agregar Permiso</button></td>';
              html += '</tr>';
          });
          $('#subOpciones').html(html);
          $('[data-toggle="tooltip"]').tooltip(); //activar el tooltip
          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la petición ajax

      });//fin del evento change

      $('#subOpciones').on('click', '.agregarPermiso', function(event) {
        event.preventDefault();
        var nombre =$(this).data('nombre');
        var id = $(this).data('id');
        var html = '';
        var b = false;
        $('input[name="permiso[]"]').each(function(index, el) {
          if ($(this).val() == id) {
            b = true;
          }
        });//fin del each
        if (b == false) {
          html += '<div class="col-sm-6"><div class="alert alert-success alert-dismissible" role="alert">';
          html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
          html += '<strong>'+nombre+'</strong><input type="hidden" name="permiso[]" value="'+id+'">';
          html += '</div></div>';
          $('#permisosRoles').append(html);
        }//fin del if
      });

      $('#registrarPermisos').click(function(event) {
        event.preventDefault();
        if ($('input[name="permiso[]"]').length != 0) {

        var rol = $('#idRol').val();
        var token = $('#token').val();
        var url = protocolo+'//'+URLdomain+'/roles/'+rol;
        jsonPermisos = [];
        //json de los curso asignados
        $('input[name="permiso[]"]').each(function(index, el) {
            console.log($(this).val());
            item = {}
            item ["permiso"] = $(this).val();
            jsonPermisos.push(item);
        });

        //convertir a string json los jsons
        var permisos = JSON.stringify(jsonPermisos);
        $.ajax({
          url: url,
          type: 'PUT',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {permisos: permisos}
        })
        .success(function(data) {
          var msg = '';
          var datos = [];
          $(data).each(function(index, el) {
              msg = el.mensaje;
              datos = el.permisos;
          });
          console.log(msg);
          var html = '';
          for (var i = 0; i < datos.length; i++) {
            html += '<tr>';
            html += '<td>'+(i+1)+'</td>';
            html += '<td>'+datos[i].name.toUpperCase()+'</td>';
            if (datos[i].state_permission_role == true) {
              html += '<td>  <input type="checkbox" name="estado" checked value="'+datos[i].state_permission_role+'" class="toggleEstado"> </td>';
            } else {
              html += '<td>  <input type="checkbox" name="estado" value="'+datos[i].state_permission_role+'" class="toggleEstado"> </td>';
            }
            html += '</tr>';
          }
          /*agregar un toggle al checkbox que tenga la clase toggleEstado*/
          $('.toggleEstado').bootstrapToggle({
            on: 'Habilitado',
            off: 'Deshabilitado',
            onstyle: 'success',
            offstyle: 'danger'
          });//fin del toggle
          $('#subOpciones').html('');
          $('#formModal').modal('hide');
          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la peticion ajax
      } else {
          alert('No hay nada que registrar, no hay nuevos permisos agregados!!!');
      }

      });//fin del evento click

      $('#datosPermisosAsignados').on('change', '.toggleEstado', function(event) {
        event.preventDefault();
        var id = $(this).val();
        var estado = $(this).prop('checked');
        var token = $('#token').val();
        var url = protocolo+'//'+URLdomain+'/permisosroles/'+id;
        var activo = 0;
        if (estado == true) {
          activo = 0;
        }else{
          activo = 1;
        }
        $.ajax({
          url: url,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {estado: activo}
        })
        .success(function(data) {
          console.log(data);
          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la peticion ajax.

      });//fin del evento on click

      $('#datosRoles').on('change', '.toggleEstado', function(event) {
        event.preventDefault();
        var id = $(this).val();
        var activo = 0;
        var url = protocolo+'//'+URLdomain+'/roles/'+id;
        var token = $('#token').val();
        if ($(this).prop('checked') == true) {
          activo = 0;
        } else {
          activo = 1;
        }
        $.ajax({
          url: url,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {estado: activo}
        })
        .done(function(data) {
          console.log(data);
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      });//fin del evento on change

      $('#datosPersonas').on('change', '.toggleEstado', function(event) {
        event.preventDefault();
        var id = $(this).val();
        var url = protocolo + '//' + URLdomain +'/personas/'+id;
        var estado = 0;
        var token = $('#token').val();
        if ($(this).prop('checked') == true) {
          estado = 0;
        } else {
          estado = 1;
        }//fin del if else

        $.ajax({
          url: url,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {estado: estado}
        })
        .success(function(data) {
          console.log("success");
        })
        .error(function() {
          console.log("error");
        });//fin de la petición ajax

      });//fin del evento on change

      $('#nAsignacionDocente').change(function(event) {
        event.preventDefault();
        var jornada = $('#jAsignacionDocente').val();
        var nivel = $(this).val();
        var plan = $('#pAsignacionDocente').val();
        var url = protocolo+'//'+URLdomain+'/asignaciondocente/grados';
        $('#gAsignacionDocente').html('<option selected="selected" value="">Seleccione un grado...</option>');
        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          data: {plan: plan, jornada: jornada, nivel: nivel}
        })
        .done(function(data) {
          console.log('Hola');
          console.log(data);
          var html = '<option selected="selected" value="">Seleccione un grado...</option>';
          $(data).each(function(index, el) {
            html += '<optgroup label="Sección: '+el.nombre_seccion.toUpperCase()+'">';
            html += '<option value="'+el.id_nivel_grado+'">'+el.nombre_grado.toUpperCase()+' '+el.nombre_carrera.toUpperCase()+'</option>';
            html += '</optgroup>';
          });
          $('#gAsignacionDocente').html(html);
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        });

      });

      $('#gAsignacionDocente').change(function(event) {
        event.preventDefault();
        var grado = $(this).val();
        if (grado == '') {
          alert('No se ha seleccionado un grado!!!');
        } else {
          var url = protocolo+'//'+URLdomain+'/pensum/pensumgrado';
          $('#pensumGrado').html('');
          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: {grado: grado}
          })
          .done(function(data) {
            if (data.length != 0) {
              var html = '<table class="table table-hover">';
              html += '<thead>';
              html += '<tr>';
              html += '<th>NO</th> <th>CURSO</th> <th>ASIGNAR</th>';
              html += '</tr>';
              html += '</thead>';
              html += '<tbody>';
              $(data).each(function(index, el) {
                  html += '<tr>';
                  html += '<td>'+(index+1)+'</td>';
                  html += '<td>'+el.nombre_area.toUpperCase()+'</td>';
                  html += '<td><button type="button" name="button" class="btn btn-xs btn-info asigCursoDocente" value="'+el.id_asignacion_area+'" data-curso="'+el.nombre_area.toUpperCase()+'"> Asignar Curso</button></td>';
                  html += '</tr>';
              });/*fin del el each*/
              html += '</tbody>';
              html += '</table>';
            } else {
              var html = '<h3 class="text-center text-danger">No se ha registrado pensum para este grado!!!</h3>'
            }/*fin del if else*/

            $('#pensumGrado').html(html);
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          });/*fin de la petición ajax*/

        }/*fin del if else*/
      });/*fin del evento change*/

      $('#pensumGrado').on('click', '.asigCursoDocente', function(event) {
        event.preventDefault();
        var id = $(this).val();
        var b = false;
        $('input[name="curso[]"]').each(function(index, el) {
            if (id == $(this).val()) {
              b = true;
            }
        });/*fin del each*/
        if (b == false) {
          var html = '<div class="alert alert-info alert-dismissible" role="alert">';
          html +='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
          html += '<strong>'+$(this).data('curso')+'</strong> <input type="hidden" name="curso[]" value="'+id+'">';
          html += '</div>';
          $('#countCursos').text($('input[name="curso[]"]').length+1);
          $('#asignacionDocente').append(html);
        }/*fin dle if*/

      });/*fin del evento on click*/

      $('#asignacionDocenteCursos').on('change', '.toggleEstado', function(event) {
        event.preventDefault();
        var id = $(this).val();
        var url = protocolo + '//' + URLdomain +'/asignaciondocente/'+id;
        var estado = 0;
        var token = $('#token').val();
        if ($(this).prop('checked') == true) {
          estado = 0;
        } else {
          estado = 1;
        }//fin del if else

        $.ajax({
          url: url,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': token},
          dataType: 'json',
          data: {estado: estado}
        })
        .done(function(data) {
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        });//fin dle evento ajax


      });//fin del evento on click

      $('#rNADocente').click(function(event) {
        event.preventDefault();
        if ($('input[name="curso[]"]').length == 0) {
          console.log('No hay cursos que asignar al docente');
          $.alert({
            title: 'Error!!!',
            content: 'No se han seleccionado cursos que asignar al docente!!!',
            confirmButton: 'OK',
            theme: 'black'
        });
        }else {
          var arrayCursos = [];
          $('input[name="curso[]"]').each(function(index, el) {
              item = {};
              item['curso'] = $(this).val();
              arrayCursos.push(item);
          });/*fin del each*/
          var cursos = JSON.stringify(arrayCursos);
          var id = $('#idPersona').val();
          var url = protocolo+'//'+URLdomain+'/asignaciondocente/'+id;
          var token = $('#token').val();
          $.ajax({
            url: url,
            type: 'PUT',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {cursos: cursos}
          })
          .done(function(data) {
            console.log(data);
            console.log("success");
            $('#myModal').modal('hide');
          })
          .fail(function() {
            console.log("error");
            $('#myModal').modal('hide');
            $.alert({
              title: 'Error!!!',
              content: 'Lo sentimos a ocurrido un error al relizar la transacción!!',
              confirmButton: 'OK',
              theme: 'black'
          });
          });//fin de la petición ajax


        }//fin del if else

      });//fin del evento click

      $('#rNADocente').click(function(event) {
        event.preventDefault();
        if ($('input[name="curso[]"]').length == 0) {
          console.log('No hay cursos que asignar al docente');
          $.alert({
            title: 'Error!!!',
            content: 'No se han seleccionado cursos que asignar al docente!!!',
            confirmButton: 'OK',
            theme: 'black'
        });
        }else {
          var arrayCursos = [];
          $('input[name="curso[]"]').each(function(index, el) {
              item = {};
              item['curso'] = $(this).val();
              arrayCursos.push(item);
          });/*fin del each*/
          var cursos = JSON.stringify(arrayCursos);
          var id = $('#idPersona').val();
          var url = protocolo+'//'+URLdomain+'/asignaciondocente/'+id;
          var token = $('#token').val();
          $.ajax({
            url: url,
            type: 'PUT',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {cursos: cursos}
          })
          .done(function(data) {
            console.log(data);
            console.log("success");
            $('#myModal').modal('hide');
          })
          .fail(function() {
            console.log("error");
            $('#myModal').modal('hide');
            $.alert({
              title: 'Error!!!',
              content: 'Lo sentimos a ocurrido un error al relizar la transacción!!',
              confirmButton: 'OK',
              theme: 'black'
          });
          });//fin de la petición ajax


        }//fin del if else

      });//fin del evento click

      $('#nIEstudiante').change(function(event) {
        event.preventDefault();
        var plan = $('select[name="id_plan"]').val();
        var jornada = $('select[name="id_jornada"]').val();
        var nivel = $(this).val();

        if (plan != '' && jornada != '' && nivel != '') {
          $('select[name="id_grado"]').html('<option selected="selected" value="">Seleccione un grado...</option>');
          var url = protocolo+'//'+URLdomain+'/asignaciondocente/grados';

          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: {plan: plan, jornada: jornada, nivel: nivel}
          })
          .done(function(data) {

            var html = '<option selected="selected" value="">Seleccione un grado...</option>';
            $(data).each(function(index, el) {
              html += '<optgroup label="Sección: '+el.nombre_seccion.toUpperCase()+'">';
              html += '<option value="'+el.id_nivel_grado+'">'+el.nombre_grado.toUpperCase()+' '+el.nombre_carrera.toUpperCase()+'</option>';
              html += '</optgroup>';
            });
            $('select[name="id_grado"]').html(html);
            console.log("success");
          })
          .fail(function() {
            console.log("error");
            $.alert({
              title: 'Error!!!',
              content: 'Lo sentimos a ocurrido un error al realizar la petición al servidor!!!',
              confirmButton: 'OK',
              theme: 'black'
          });//fin del alert

          });//fin del la petición ajax


        } else {
          $.alert({
            title: 'Error!!!',
            content: 'Es necesario que seleccione un plan, una jornada y un nivel!!!',
            confirmButton: 'OK',
            theme: 'black'
        });//fin del alert

      }//fin del if else

      });//fin del evento click

      $('select[name="id_departamento"]').change(function(event) {
        event.preventDefault();
          var id = $(this).val();
          if (id == '') {
                $.alert({
                  title: 'Error!!!',
                  content: 'Es necesario que seleccione un departamento!!!',
                  confirmButton: 'OK',
                  theme: 'black'
              });//fin del alert
          } else {
            var url = protocolo +'//'+URLdomain+'/municipios';
            $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              data: {id: id}
            })
            .done(function(data) {
              var html = '<option selected="selected" value="">Seleccione municipio...</option>';
              $(data).each(function(index, el) {
                html += '<option value="'+el.id_municipio+'">'+el.nombre_municipio.toUpperCase()+'</option>';
              });//fin del each
              $('select[name="id_municipio"]').html(html);
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            });//fin de la petición ajax

          }//fin del if else
      });//fin del evento change

      /*$('#agregarTutor').click(function(event) {
        event.preventDefault();
        var html = $('.campos-tutores').html();
        $('.campos-tutores').append(html);
      });*/

      /* el calendario */
    $(function () {
        $('.calendario').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD',
            locale: 'es'
        });
    });//fin del calendario

    /*agregar un toggle al checkbox que tenga la clase toggleEstado*/
    $('.toggleEstado').bootstrapToggle({
      on: 'Habilitado',
      off: 'Deshabilitado',
      onstyle: 'success',
      offstyle: 'danger'
    });//fin del toggle

  /*
      Autocompletado para buscar a una persona que tenga el puesto docente
    */
    $('#autocompleteDocente').autocomplete({
      serviceUrl: protocolo+'//'+URLdomain+'/personas/autocomplete/docente',//url donde se va a hacer la peticion de las sugerencias
      onSelect: function(suggestion) {
          //$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
          $(this).data('id', suggestion.data);
          $('#idPersona').val(suggestion.data);
          //console.log(suggestion.data);
      },
      onInvalidateSelection: function() {
          //$('#selction-ajax').html('You selected: none');
          //console.log('You selected: none');
          $(this).data('id', '');
          $('#idPersona').val('');
      }
    });
  /*Fin del autocomplete*/

/* Aisgnar el autocomplete*/
  $('#autocomplete').autocomplete({
    serviceUrl: protocolo+'//'+URLdomain+'/areas/autocomplete',//url donde se va a hacer la peticion de las sugerencias
    onSelect: function(suggestion) {
        //$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
        $(this).data('id', suggestion.data);
        //console.log(suggestion.data);
    },
    onInvalidateSelection: function() {
        //$('#selction-ajax').html('You selected: none');
        //console.log('You selected: none');
        $(this).data('id', '');
    }
  });
/*Fin del autocomplete*/

/* Autocompletado de busqueda de persona*/
$('#busquedaPersona').autocomplete({
  serviceUrl: protocolo+'//'+URLdomain+'/personas/autocomplete',//url donde se va a hacer la peticion de las sugerencias
  onSelect: function(suggestion) {
      //$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
      //$(this).data('id', suggestion.data);
      //console.log(suggestion.data);
      $('#idPersona').val(suggestion.data);
  },
  onInvalidateSelection: function() {
      //$('#selction-ajax').html('You selected: none');
      //console.log('You selected: none');
      //$(this).data('id', '');
      $('#idPersona').val('');
  }
});
/*CJ Fin del autocompletado de busqueda de persona*/

/* Autocompletado de busqueda de persona*/
$('#buscaPersona').autocomplete({
  serviceUrl: protocolo+'//'+URLdomain+'/verusuario/autocomplete',//url donde se va a hacer la peticion de las sugerencias
  onSelect: function(suggestion) {
      //$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
      //$(this).data('id', suggestion.data);
      //console.log(suggestion.data);
      $('#idPersona').val(suggestion.data);
  },
  onInvalidateSelection: function() {
      //$('#selction-ajax').html('You selected: none');
      //console.log('You selected: none');
      //$(this).data('id', '');
      $('#idPersona').val('');
  }
});
/*Fin del autocompletado de busqueda de persona*/

// MODAL INSERT
$.ajaxSetup(
{
    headers:
    {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
});
$('#add').on('click', function(){
  $(#usuario).modal('show');
})

$('#frmUsuario').on('submit',function(e){
  e.preventDefault();
  var form=$('#frmUsuario');
  var formData=form.serialize();
  var url = protocolo +'//'+URLdomain+'/newUsuario';
  $.ajax({
    type:'post',
    url: url,
    headers: {'X-CSRF-TOKEN': token},
    data: formData,
    async: true,
    dataType : 'json',
    success:function(data)
    {
      console.log(data);
    }
  });
})



});//fin del document ready