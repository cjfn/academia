
@extends('layouts.app1')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>SISTEMA CONTROL DE PAGOS</h1></div>

                <div class="panel-body">
                <h3>
                    <strong> Bienvenido </strong>a la aplicacion en la cual podra administrar y gestionar su institucion<br>
                    <li><span class="fa fa-user fa-2x"></span>Crear nuevos <strong>usuarios</strong> con permisos<br></li>
                    <li><span class="fa fa-graduation-cap fa-2x"></span>Crear y administrar <strong>Alumnos</strong> <br></li>
                    <li><span class="fa fa-book fa-2x"></span>cursos<br></li>
                    <li><span class="fa fa-money fa-2x"></span>pagos<br></li>
                    <center>
                    <a href="{{ url('/home') }}" class="btn btn-success"><span class="fa fa-unlock fa-3x"></span>Ingresa</a>
                     </center>
                </h3>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
<center>
<h3>Powered by<strong> <a href="http://dsolucionesit.com/">dsolucionesit.com</a></strong></h3>
<h3><span class="fa fa-email"></span>cjfn10101@gmail.com</h3>
</center>
</footer>
@endsection
