{!! csrf_field() !!}
<div class="form-group">

  {!!Form::label('persona', 'Persona*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Buscar persona...', 'id'=>'name', 'required'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('email', 'Email*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email...', 'id'=>'email', 'required'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('contraseña', 'Contraseña*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::password('password', null, ['class'=>'form-control', 'id'=>'password', 'required'])!!}

  </div>

</div>


<div class="form-group">

  {!!Form::label('rol', 'Rol*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::select('tipo_usuario', $roles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione rol...'])!!}

  </div>

</div>

<div class="form-group">

  <div class="col-sm-offset-2 col-sm-10">

    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>

    {!!Form::submit('Registrar', ['class' => 'btn btn-info'])!!}

  </div>

</div>

