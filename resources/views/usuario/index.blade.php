

@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />


<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">

             

            @include('partials.messages')
            

    <div class="panel panel-default">
            @include('partials.messages')
                <div class="panel-heading">TUS USUARIOS
                </div>
    <div class="panel-body">
    <h2>Administra los usuarios que tienen acceso a tu sistema</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
   
    <table class="table table-bordered">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          <th>codigo</th>
          <th>nombre</th>
          <th>correo</th>
          <th>password</th>
          <th>tipo de usuario</th>
          <th>opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> id}}</td>
          <td>{{$x -> name}}</td>
          <td>{{$x -> email}}</td>
          <td>************</td>
          <td>{{$x -> display_name}}</td>
           <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" title="Opcion desabilitada"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('usuario/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('usuario/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Record</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuario') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="name" name="name" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Email:</label>
                  <input type="email" class="form-control" id="email" name="email" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Contraseña:</label>
                  <input type="password" class="form-control" id="password" name="password" required="true">
                </div>
              
                 </div>
                <label for="email">tipo de usuario</label>
               <select id="tipo_usuario" name="tipo_usuario" required="true"  class="form-control">
                  @foreach($roles as $r)
                  
                      <option value="{{ $r -> id}}">{{ $r -> name}}</option>
                  
                  @endforeach
                </select>
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">View</h4>
          </div>
          <div class="modal-body">
            <p><b>nombre : </b><span id="view_name" class="text-success"></span></p>
            <p><b>email : </b><span id="view_email" class="text-success"></span></p>
            <p><b>password : </b><span id="view_password" class="text-success"></span></p>
            <p><b>tipo_usuario : </b><span id="view_tipo_usuario" class="text-success"></span></p>
           
              
            <select id="view_tipo_usuario" >
             @foreach($roles as $r)
                <option value="{{ $r -> id}}">{{ $r -> name}}</option>
              @endforeach
            </select>
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuario/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_name" name="edit_name">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Email :</label>
                  <input type="email" class="form-control" id="edit_email" name="edit_email">
                </div>

                <div class="form-group">
                  <label for="edit_descripcion">Contraseña :</label>
                  <input type="text" class="form-control" id="edit_password" name="edit_password">
                </div>
                <label for="edit_tipo_usuario">Tipo de Usuario:</label>
                <select id="tipo_usuario" name="tipo_usuario" required="true"  class="form-control">
                  @foreach($roles as $r)
                  
                      <option value="{{ $r -> id}}">{{ $r -> name}}</option>
                  
                  @endforeach
                </select>
              </div>
              
              <button type="submit" class="btn btn-default">Update</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>
  <script type="text/javascript">
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_name").text(result.name);
          $("#view_email").text(result.email);
          $("#view_password").text(result.password);
          $("#view_tipo_usuario").text(result.tipo_usuario);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_name").val(result.name);
          $("#edit_email").val(result.email);
          $("#edit_password").val(result.password);
          $("#edit_tipo_usuario").val(result.tipo_usuario);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Are you sure want to delete??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

