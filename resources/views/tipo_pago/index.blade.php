
@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />


<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">

             

              @include('partials.messages')
              
    <div class="panel panel-default">
            @include('partials.messages')
                <div class="panel-heading">TIPOS DE PAGOS DE TU ACADEMIA
                </div>
    <div class="panel-body">
    <h2>Administra tus tipos de pagos</h2>
    <div class="panel panel-success">
      <div class="panel-heading"><span class="fa fa-info"></span>INFORMACION </div>
      <div class="panel-body">
        Administra los tipos de pagos que se iran haciendo, a lo largo del año, recuerda que al finalizar el año deberas de crear cursos nuevos.
      </div>
    </div>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
   
    <table class="table table-bordered" id="MyTable">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          
          <th>Nombre</th>
          <th>Curso</th>
          <th>Pago Q.</th>
          <th>tipo de pago</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>

          <td>{{$x -> nombre}}</td>
          <td>{{$x -> nombre_curso}}</td>
          <td>{{$x -> pago}}</td>
          <td>{{$x -> tipo_pago}}</td>
          <td>
             <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('tipos_pagos/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('tipos_pagos/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregue tipo de pago </h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('tipos_pagos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" title="Agregue el nombre del pago ej: pago enero" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Tipo de pago:</label>
                
                  <select class="form-control" title="Ejemplo: pago mensual, bimestral, semestral o anual" id="tipo_pago" name="tipo_pago">
                    <option value="MENSUAL">MENSUAL</option>
                    <option value="BIMESTRAL">BIMESTRAL</option>
                    <option value="TRIMESTRAL">TRIMESTRAL</option>
                    <option value="SEMESTRAL">ANUAL</option>
                    <option value="PAGO UNICO">PAGO UNICO</option>
                    <option value="OTRO">OTRO</option>
                  </select>
                </div>
               <div class="form-group">
                  <label for="last_name">Pago Q.:</label>
                  <input type="number" name="pago" id="pago" class="form-control">
                </div>
                 <div class="form-group">
                  <label for="last_name">Curso :</label>
                   <select id="curso" name="curso" required="true"  class="form-control">
                      @foreach($cursos as $r)
                      <option value="{{ $r -> id}}"> {{ $r -> nombre}}    </option>
                  @endforeach
                   </select>
                </div>
               
                <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <textarea class="form-control" id="Observaciones" name="observaciones" >
                  </textarea>
                </div>
              </div>
              
              <button type="submit" class="btn btn-default"><span class="fa fa-plus"></span>Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Tipo de Pago : </b><span id="view_tipo_de_pago" class="text-success"></span></p>
            <p><b>Pago : Q.</b><span id="view_pago" class="text-success"></span></p>
            <p><b>Observaciones : </b><span id="view_observaciones" class="text-success"></span></p>
            <p><b>Fecha de creación : </b><span id="view_fecha_created" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('tipos_pagos/update') }}" method="PUT">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_tipo_de_pago">Tipo De Pago :</label>
                    <select class="form-control" title="Ejemplo: pago mensual, bimestral, semestral o anual" id="edit_tipo_pago" name="edit_tipo_pago">
                    <option value="MENSUAL">MENSUAL</option>
                    <option value="BIMESTRAL">BIMESTRAL</option>
                    <option value="TRIMESTRAL">TRIMESTRAL</option>
                    <option value="SEMESTRAL">ANUAL</option>
                    <option value="PAGO UNICO">PAGO UNICO</option>
                    <option value="OTRO">OTRO</option>
                  </select>
                </div>
              <div class="form-group">
                  <label for="edit_precio_anio">Pago Q.:</label>
                  <input type="number" class="form-control" id="edit_pago" name="editp_pago">
                </div>
                <div class="form-group">
                  <label for="edit_precio_anio">Año:</label>
                  <input type="number" class="form-control" id="edit_anio" name="edit_anio">
                </div>
                <div class="form-group">
                  <label for="edit_precio_curso">Observaciones:</label>
                  <textarea  class="form-control" id="edit_observaciones" name="edit_observaciones">
                    </textarea>
                </div>
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>Close</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

<link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
<script language="javascript" type="text/javascript">
              
$(document).ready(function () {
  $('#MyTable').DataTable();
    $('#addModal').on('shown.bs.modal', function() {
    $('#nombre').focus();
  });
  });

    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //alert(result);
          $("#view_nombre").text(result.nombre);
          $("#view_tipo_de_pago").text(result.tipo_pago);
          $("#view_pago").text(result.pago);
          $("#view_observaciones").text(result.bservaciones);
          $("#view_fecha_created").text(result.fecha_created);
        },
         error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
    }   
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result.id);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_pago").val(result.pago);
          $("#edit_tipo_de_pago").val(result.tipo_de_pago);
          $("#edit_observaciones").val(result.Observaciones);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Are you sure want to delete??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"PUT", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>
  </div>        <!-- /.col-lg-12 -->
</div>
@endsection

