

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">

    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')

   

    </div>
        <div class="col-md-8">
            <h1 class="page-header text-center">NUEVO CURSO PARA {{ $nombre }} {{ $apellido }} {{ $grado }} </h1>
        </div>
        <div class="col-md-8">
        
        <div class="panel panel-default">
        <div class="panel panel-default"> 


        {!!Form::open(['route'=>'cursos_de_alumnos.store','method'=>'POST'])!!}
        {{ Form::hidden('alumno', $alumno, array('id' => $alumno)) }}
        	<div class="from-group">
        	 <select id="curso" name="curso" required="true"  class="form-control">
                      @foreach($cursos as $c)
                  
                      <option value="{{ $c -> id}}">{{ $c -> nombre}} </option>
                  
                  @endforeach
              </select>
        	</div>

           

        	<div class="from-group">
                 
                {!!Form::label('Hora de inicio *')!!}
                <select  class="form-control" name="hora_inicio" id="jornada">
                 <option value="08:00:00">8:00 am</option>
                 <option value="09:00:00">9:00 am</option>
                 <option value="10:00:00">10:00 am</option>
                 <option value="11:00:00">11:00 am</option>
                 <option value="12:00:00">12:00 am</option>
                 <option value="13:00:00">1:00 pm</option>
                 <option value="14:00:00">2:00 pm</option>
                 <option value="15:00:00">3:00 pm</option>
                 <option value="16:00:00">4:00 pm</option>
                 <option value="17:00:00">5:00 pm</option>
                 <option value="18:00:00">6:00 pm</option>
                </select>
            </div>
            <center>



            
            <div class="from-group">
                 
                {!!Form::label('Hora de fin *')!!}
                <select  class="form-control" name="hora_fin" id="jornada">
                 <option value="08:00:00">8:00 am</option>
                 <option value="09:00:00">9:00 am</option>
                 <option value="10:00:00">10:00 am</option>
                 <option value="11:00:00">11:00 am</option>
                 <option value="12:00:00">12:00 am</option>
                 <option value="13:00:00">1:00 pm</option>
                 <option value="14:00:00">2:00 pm</option>
                 <option value="15:00:00">3:00 pm</option>
                 <option value="16:00:00">4:00 pm</option>
                 <option value="17:00:00">5:00 pm</option>
                 <option value="18:00:00">6:00 pm</option>
                </select>
            </div>
            <center>

            <div class="from-group">
            {!!Form::label('Jornada *')!!}
            <select  class="form-control" name="jornada" id="jornada">
             <option value="1">Diario</option>
              <option value="2">Fin de semana</option>
            </select>
            </div>
            <center>

        	{!!Form::submit('Crear nuevo',['name'=>'grabar','id'=>'grabar','class'=>'btn btn-success fa fa-plus'])!!}

            </center>
            <br>
        {!!Form::close()!!}
        </div>

        <div class="col-md-4">
        </div>
        </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {


})


</script>
@endsection