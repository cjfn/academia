

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')


    </div>
        <div class="col-md-5 panel panel-default">
        <div class="panel-body">
            <h3 class="text-center panel panel-header">DETALLES ALUMNO </h3>
        
        </div>

        {!!Form::model($alumno, ['route'=>['alumnos.destroy',$alumno->id],'method'=>'POST'])!!}

        	<div class="form-group">
        	
        	</div>

        	
        	
        	<div class="form-group">
        	{!!form::label('alumno')!!}
        	{!!$alumno->nombre!!}

        	</div>

            <div class="form-group">
            {!!form::label('apellido')!!}
            {!!$alumno->apellido!!}

            </div>

            <div class="form-group">
            {!!form::label('direccion')!!}
            {!!$alumno->direccion!!}

            </div>

            <div class="form-group">
            {!!form::label('fecha de nacimiento')!!}
            {!!$alumno->fecha_nacimiento!!}

            </div>

            <div class="form-group">
            {!!form::label('ciclo')!!}
            {!!$alumno->ciclo!!}

            </div>

             <div class="form-group">
            {!!form::label('departamento')!!}
            {!!$alumno->depto!!}

            </div>

             <div class="form-group">
            {!!form::label('municipio')!!}
            {!!$alumno->mun!!}

            </div>

             <div class="form-group">
            {!!form::label('establecimiento')!!}
            {!!$alumno->esta!!}

            </div>
           
           
             

        	<div class="form-group">
        
        	</div>
            </div>
            <br>
            <br>
            <br>
            <div class="col-md-4 alert alert-warning alert-dismissable"> 
              <div class="form-group panel panel-default">
            <h3 class="text-center panel panel-header">
            {!!form::label('Cursos Asignados')!!}
            </h3>
            <h4 class="text-center">
             @foreach($cursos as $c)
             <strong>Curso: {{$c->nombre}}</strong> Horario: {{$c->hora_inicio}} a {{$c->hora_fin}}  <br>
            @endforeach
            </h4>
            </div>
            
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        	   <h2 class="text-center"><span class="fa fa-plus"></span> <span class="fa fa-book"></span> DESEA AGREGAR  CURSOS A ESTE ALUMNO </h2>
               <center>
                 
            	<button type="button" id="cancelar" name="cancelar" class="btn btn-default ">Cancelar</button>
                </div>
                </center>
        {!!Form::close()!!}
        

    </div>
</div>
@endsection