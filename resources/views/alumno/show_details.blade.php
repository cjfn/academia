

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')


    </div>
        <div class="col-md-8 ">
        <h3>eliminar ALUMNO </h3>
        {!!Form::model($alumno, ['route'=>['alumnos.destroy',$alumno->id],'method'=>'DELETE'])!!}

        	<div class="form-group">
        	<label for="exampleInputPassword1"> DETALLES </label>
        	</div>

        	<div class="form-group">
        	{!!form::label('id')!!}
        	{!!$alumno->id!!}

        	</div>
        	
        	<div class="form-group">
        	{!!form::label('alumno')!!}
        	{!!$alumno->nombre!!}

        	</div>
        	<div class="form-group">
        	<label for="exampleInputPassword1"> DESEA ELIMINAR ESTE REGISTRO: </label>
        	</div>
        	
        	{!!Form::submit('Eliminar',['name'=>'grabar','id'=>'grabar','content'=>'<span>Eliminar</span>','class'=>'btn btn-warning btn-sm m-t-10'])!!}
        	<button type="button" id="cancelar" name="cancelar" class="btn btn-default btn-sm m-t-10">Cancelar</button>

        {!!Form::close()!!}
        </div>

    </div>
</div>
@endsection