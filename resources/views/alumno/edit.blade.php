

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')


    </div>
        <div class="col-md-8 ">
        <h3>EDITAR ALUMNO </h3>
        {!!Form::model($alumno, ['route'=>['alumnos.update',$alumno->id],'method'=>'PUT'])!!}


        	<div class="from-group">
        	{!!Form::label('Nombre')!!}
        	{!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Nombre'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('Apellido')!!}
        	{!!Form::text('apellido',null,['id'=>'apellido','class'=>'form-control','placeholder'=>'Apellido'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('clave')!!}
        	{!!Form::number('clave',null,['id'=>'clave','class'=>'form-control','placeholder'=>'clave'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('Departamento')!!}
        	 {!!Form::select('departamento', $departamentos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione departamento...'])!!}

        	</div>

        	<div class="from-group">
        	{!!Form::label('Municipio')!!}
			{!!Form::select('municipio', $municipio, null, ['class'=>'form-control', 'placeholder'=>'Seleccione municipio...'])!!}
			</div>

        	<div class="from-group">
        	{!!Form::label('Establecimiento')!!}
        	{!!Form::select('establecimiento', $establecimiento, null, ['class'=>'form-control', 'placeholder'=>'Seleccione establecimiento...'])!!}

        	</div>

        	<div class="from-group">
        	{!!Form::label('telefono')!!}
        	{!!Form::number('telefono',null,['id'=>'telefono','class'=>'form-control','placeholder'=>'establecimiento'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('direccion')!!}
        	{!!Form::text('direccion',null,['id'=>'direccion','class'=>'form-control','placeholder'=>'establecimiento'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('fecha de nacimiento')!!}
        	{!!Form::date('fecha_nacimiento',null,['id'=>'fecha_nacimiento','class'=>'form-control','placeholder'=>'establecimiento'])!!}
        	</div>

        	{!!Form::submit('Grabar',['name'=>'grabar','id'=>'grabar','content'=>'<span>Grabar</span>','class'=>'btn btn-warning btn-sm m-t-10'])!!}


        {!!Form::close()!!}
        </div>

    </div>
</div>
@endsection