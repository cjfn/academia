

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">

    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')

   

    </div>
        <div class="col-md-9">
            <h1 class="page-header text-center">NUEVO ALUMNO </h1>
        </div>
        <div class="col-md-5">
        
        {!!Form::open(['route'=>'alumnos.store','method'=>'POST'])!!}
        	<div class="from-group">
        	{!!Form::label('Nombre *')!!}
        	{!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Nombre','required' => 'required'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('Apellido *')!!}
        	{!!Form::text('apellido',null,['id'=>'apellido','class'=>'form-control','placeholder'=>'Apellido','required' => 'required'])!!}
        	</div>

            <div class="from-group">
            {!!Form::label('Ciclo *')!!}
            <select  class="form-control" name="ciclo" id="ciclo">
              <option value="">Seleccione un año...</option>
             <option value="2016">2016</option>
              <option value="2017">2017</option>
              <option value="2018">2018</option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
              <option value="2023">2023</option>
              <option value="2024">2024</option>
              <option value="2023">2025</option>
              <option value="2024">2026</option>
              <option value="2023">2027</option>
              <option value="2024">2028</option>
              <option value="2023">2029</option>
              <option value="2024">2030</option>


            </select>
            </div>

        	

        	<div class="from-group">
        	{!!Form::label('Departamento *')!!}
        	 {!!Form::select('departamento', $departamentos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione departamento...'])!!}

        	</div>

        	<div class="from-group">
        	{!!Form::label('Municipio *')!!}
			{!!Form::select('municipio', $municipio, null, ['class'=>'form-control', 'placeholder'=>'Seleccione municipio...'])!!}
            
			</div>
        </div>
    
        <div class="col-md-4">
 
        	<div class="from-group">
        	{!!Form::label('Establecimiento *')!!}
        	{!!Form::select('establecimiento', $establecimiento, null, ['class'=>'form-control', 'placeholder'=>'Seleccione establecimiento ...'])!!}

        	</div>

        	<div class="from-group">
        	{!!Form::label('telefono *')!!}
        	{!!Form::number('telefono',null,['id'=>'telefono','class'=>'form-control','placeholder'=>'establecimiento','required' => 'required'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('direccion *')!!}
        	{!!Form::text('direccion',null,['id'=>'direccion','class'=>'form-control','placeholder'=>'establecimiento', 'required' => 'required'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('fecha de nacimiento *')!!}
        	{!!Form::date('fecha_nacimiento',null,['id'=>'fecha_nacimiento','class'=>'form-control','placeholder'=>'establecimiento', 'required' => 'required'])!!}
        	</div>

            <div class="from-group">
            {!!Form::label('Grado')!!}
          <select  class="form-control" name="grado" id="grado">
             <option value="PRIMERO PRIMARIA">PRIMERO PRIMARIA</option>
              <option value="SEGUNDO PRIMARIA">SEGUNDO PRIMARIA</option>
              <option value="TERCERO PRIMARIA">TERCERO PRIMARIA</option>
              <option value="CUARTO PRIMARIA">CUARTO PRIMARIA</option>
              <option value="QUINTO PRIMARIA">QUINTO PRIMARIA</option>
              <option value="SEXTO PRIMARIA">SEXTO PRIMARIA</option>
              <option value="PRIMERO BASICO">PRIMERO BASICO</option>
              <option value="SEGUNDO BASICO">SEGUNDO BASICO</option>
              <option value="TERCERO BASICO">TERCERO BASICO</option>
              <option value="CUARTO DIVERSIFICADO">CUARTO DIVERSIFICADO</option>
              <option value="QUINTO DIVERSIFICADO">QUINTO DIVERSIFICADO</option>
              <option value="SEXTO DIVERSIFICADO">SEXTO DIVERSIFICADO</option>            
            </select>
            </div>
            <center>

        	{!!Form::submit('Crear nuevo',['name'=>'grabar','id'=>'grabar','class'=>'btn btn-success fa fa-plus'])!!}

            </center>
            <br>
        {!!Form::close()!!}
        </div>

       </div>
   </div>
</div>


    </div>
</div>
@endsection