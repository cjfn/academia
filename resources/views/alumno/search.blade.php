{!! Form::open(array('url'=>'alumnos','method'=>'GET','autocomplete'=>'off','role'=>'search')) !!}
<div class="form-group">
<br/>
  <div class="input-group">
  	<div class="container">
    <div class="row">
    	<div class="col-xs-12 col-md-5">
    		FILTRO POR AÑO
    	</div>
    	<div class="col-xs-12 col-md-2">
    
	    <select  class="form-control" id="searchYear" name="searchYear" value="{{$searchYear}}">
	     <option value="2016">2016</option>
		  <option value="2017">2017</option>
		  <option value="2018">2018</option>
		  <option value="2019">2019</option>
		  <option value="2020">2020</option>
		  <option value="2021">2021</option>
		  <option value="2022">2022</option>
		  <option value="2023">2023</option>
		  <option value="2024">2024</option>
		  <option value="2025">2025</option>
		  <option value="2026">2026</option>
		  <option value="2027">2027</option>
		  <option value="2028">2028</option>
	    </select>
	</div>


	<div class="col-xs-12 col-md-1">
    <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

    <button type="submit" class="btn btn-info"><div class="fa fa-search"></div>Buscar</button>
    </span>
	</div>
	</div>
	</div>
  </div>

</div>
<script>
$(document).ready(function() { /* code here */ });
//alert("hola");
  var currentTime = new Date()
                // returns the month (from 0 to 11)
                var month = currentTime.getMonth() + 1
                // returns the day of the month (from 1 to 31)
                var day = currentTime.getDate()
                // returns the year (four digits)
                var year = currentTime.getFullYear()
                var fecha = day+ "/"+month+"/"+year;
                // write output MM/dd/yyyy
                //alert(fecha);
                $("#searchYear").val(year);
</script>

{{Form::close()}}