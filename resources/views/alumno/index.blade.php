

@extends('layouts.app')







@section('content')


    <script type="text/javascript" src="{{ url('/') }}/js/jquery.dataTables.min.js"></script> 

    <script type="text/javascript" src="{{ url('/') }}/js/dataTables.bootstrap.min.js"></script> 

    <script type="text/javascript" src="{{ url('/') }}/js/jquery.dataTables.min.js"></script> 

    <script type="text/javascript" src="{{ url('/') }}/js/bootstrap.js"></script> 

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">

    <div class="row">

    <div class="col-md-3">

     <!-- Sidebar -->

    @include('menus.menuadmin')

    </div>

        <div class="col-md-8 ">
        <div class="container">
           <div class="row">
            <div class="col-xs-12 col-md-12">
              @include('alumno.search')
            </div>
            
            </div>
        <div class="row">
        <div class="col-xs-12 col-md-8">
              <h1 class="page-header text-center">ALUMNOS DISPONIBLES 
              @if($searchYear!=null)
              {{$searchYear}}
              @else
             
              @endif
            </h1>
              </div>
              <div class="col-xs-12 col-md-2">
             
        </div>
        </div>
        </div>
            <center>
            {!!link_to_route('alumnos.create', $title = "Nuevo Alumno", $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus fa fa-user'])!!}

            </center>

   
            @include('partials.messages')


            @if(count($alumnos) == 0)

              <p class="text-info">

                No se han registrado alumnos aun.

              </p>

            @else
           



                <div class="table-responsive">

                  <table class="table table-hover table-bordered" id="MyTable">

                    <thead>

                      <tr>

                        <th>

                          NO

                        </th>

                        <th>

                          NOMBRES


                        </th>

                        <th>

                          CICLO 

                        </th>


                        <th>

                          ESTADO 

                        </th>

                        <th>

                          OPCIONES

                        </th>

                      </tr>

                    </thead>

                    <tbody id="datosAlumnos">

                      @foreach($alumnos as $key => $rol)

                        <tr>

                          <td>

                            {{ $rol->id }}

                          </td>

                          <td>

                            {{ mb_strtoupper($rol->nombre) }} {{ mb_strtoupper($rol->apellido) }}

                          </td>

                          <td>

                            {{ mb_strtoupper($rol->ciclo) }}

                          </td>

                         

                            @if($rol->activo==1)

                            
                              <td>

                                <span class="fa fa-check"></span> ACTIVO

                              </td>

                              @else

                               <td>

                                <span class="fa fa-close"></span> INACTIVO

                              </td>

                            @endif


                            

                          

                         <td>
                         <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('alumnos/delete')}}">

                          <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('alumnos/view')}}">

                         


                            {!!link_to_route('alumnos.edit', $title = 'Modificar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}

                        

                            {!!link_to_route('alumnos.show', $title = 'Detalles', $parameters = $rol->id, $attributes = ['class'=>'btn btn-info fa fa-eye'])!!}




                            <button class="btn btn-danger btn-sm" onclick="fun_delete('{{$rol -> id}}')"><span class="fa fa-times"> </span> Eliminar</button>



                          </td>



                        





                        </tr>



                      @endforeach



                    </tbody>




                  </table>


                </div>

    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">View</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Apellido : </b><span id="view_apellido" class="text-success"></span></p>
            <p><b>Fecha de nacimiento: </b><span id="view_fecha" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();



                </script>

    <!-- view modal ends -->

                <script type="text/javascript">

window.onload = function exampleFunction() { 
  
            // Function to be executed 
                // Return today's date and time
                var currentTime = new Date()
                // returns the month (from 0 to 11)
                var month = currentTime.getMonth() + 1
                // returns the day of the month (from 1 to 31)
                var day = currentTime.getDate()
                // returns the year (four digits)
                var year = currentTime.getFullYear()
                var fecha = day+ "/"+month+"/"+year;
                // write output MM/dd/yyyy
                //alert(fecha);
                $("#mySelect").val(year);
                alert(year);
} 

    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_nombre").text(result.nombre);
          $("#view_apellido").text(result.apellido);
          $("#view_fecha").text(result.fecha_nacimiento);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_descripcion").val(result.descripcion);
          $("#edit_precio_curso").val(result.precio_curso);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Are you sure want to delete??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>


                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}





            @endif







          </div>



          <!-- /.col-lg-12 -->



      </div>

      





  </div>




@endsection



