

@extends('layouts.app')







@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />


  

<div class="container">

    <div class="row">

    <div class="col-md-3">

     <!-- Sidebar -->

    @include('menus.menuadmin')

    </div>

        <div class="col-md-8 ">
        <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8">
              <h1 class="page-header text-center">ALUMNOS DISPONIBLES </h1>
              </div>
              <div class="col-xs-12 col-md-2">
             
        </div>
        </div>
        </div>
            <center>
            {!!link_to_route('alumnos.create', $title = "Nuevo Alumno", $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus fa fa-user'])!!}

            </center>

   
            @include('partials.messages')


            @if(count($alumnos) == 0)

              <p class="text-info">

                No se han registrado alumnos aun.

              </p>

            @else
          




                <div class="table-responsive">

                  <table class="table table-hover table-bordered" id="MyTable">

                    <thead>

                      <tr>

                        <th>

                          NO

                        </th>

                        <th>

                          NOMBRES


                        </th>

                        <th>

                          TELEFONO 

                        </th>

                        <th>

                          OPCIONES

                        </th>

                      </tr>

                    </thead>

                    <tbody id="datosAlumnos">

                      @foreach($alumnos as $key => $rol)

                        <tr>

                          <td>

                            {{ $rol->id }}

                          </td>

                          <td>

                            {{ mb_strtoupper($rol->nombre) }} {{ mb_strtoupper($rol->apellido) }}

                          </td>

                         <td>

                            {{ mb_strtoupper($rol->telefono) }}

                          </td>

                         <td>
                         <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('alumnos/delete')}}">

                          <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('alumnos/view')}}">

                         

                          

                            {!!link_to_route('alumnos.edit', $title = 'Modificar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}

                        

                            {!!link_to_route('alumnos.show', $title = 'Detalles', $parameters = $rol->id, $attributes = ['class'=>'btn btn-info fa fa-eye'])!!}




                            <button class="btn btn-danger btn-sm" onclick="fun_delete('{{$rol -> id}}')"><span class="fa fa-times"> </span> Eliminar</button>



                          </td>



                        





                        </tr>



                      @endforeach



                    </tbody>




                  </table>


                </div>
                <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();
                  } );
                </script>


                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}





            @endif







          </div>



          <!-- /.col-lg-12 -->



      </div>

      





  </div>






@endsection



