
@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">
              <h1 class="page-header text-center">ALUMNOS DISPONIBLES</h1>
              
            {!!link_to_route('alumnos.create', $title = "Nuevo Alumno", $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
            <br>

            @include('partials.messages')
            <br>
            @if(count($alumnos) == 0)
              <p class="text-info">
                No se han registrado alumnos aun.
              </p>
            @else
                <div class="table-responsive">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>
                          NO
                        </th>
                        <th>
                          NOMBRE 
                        </th>
                        <th>
                          APELLIDO 
                        </th>
                        <th>
                          DEPARTAMENTO 
                        </th>
                        <th>
                          MUNICIPIO 
                        </th>
                        <th>
                          ESTABLECIMIENTO 
                        </th>
                        <th>
                          TELEFONO 
                        </th>
                         <th>
                          DIRECCION
                        </th>
                        <th>
                          ACTUALIZAR
                        </th>
                        <th>
                          ESTADO
                        </th>
                      </tr>
                    </thead>
                    <tbody id="datosAlumnos">
                      @foreach($alumnos as $key => $rol)
                        <tr>
                          <td>
                            {{ $rol->id }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->nombre) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->apellido) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->detpo) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->mun) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->estab) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->telefono) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->direccion) }}
                          </td>
                          <td>
                            {!!link_to_route('alumnos.edit', $title = 'Editar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-warning'])!!}

                          </td>


                          <td>
                            {!!link_to_route('alumnos.show', $title = 'Eliminar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-danger'])!!}

                          </td>

                          <td>

                            
                          </td>


                        </tr>

                      @endforeach

                    </tbody>

                  </table>

                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}

                </div>

            @endif



          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

