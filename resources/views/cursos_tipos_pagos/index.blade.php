

@extends('layouts.app')



@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')


    </div>
        <div class="col-md-9 panel panel-default">
        <div class="panel-body">
        @if($data==null)

        <p> no hay registros </p>

        @else
        <H2 class="text-center"> Administra los tipos de pagos de tus cursos</H2>
        

        <div class="panel panel-success">
          <div class="panel-heading"><span class="fa fa-info"></span><strong>INFORMACION </strong> </div>
          <div class="panel-body">
            Configura y agrega los pagos de tus alumnos, agrega a un curso determinada cantidad de pagos al año, recuerda esta asignacion se debe de hacer año con año
            <br>
            <strong>¿COMO UTILIZARLO?</strong>
            <li> crea un nuevo <a data-toggle="modal" data-target="#addModalcurso"><span class="fa fa-plus"></span>Curso</a> </li>
            <li> Crea un nuevo <a data-toggle="modal" data-target="#addModalTipoPagos"><span class="fa fa-plus"></span>Tipo de pago </a> </li>
            <li> Agrega un <a data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Curso a un Tipo de pago </a> </li>


          </div>
        </div>
        @if ($message = Session::get('success'))

          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
        @endif

    <table class="table table-bordered">
        <center>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agrega Pagos a  Tus cursos</button>
        </center>
          <thead>
            <tr>
              
              <th>Curso</th>
              <th>Tipo de pago</th>
              <th>Año</th>
              <th>Su Pago en Q.</th>
              <th>Observaciones</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>  
          @foreach ($data as $key=>$pagoscursos)
          <tr>
            <td>
              {{ mb_strtoupper($pagoscursos->curso_nombre) }}
            </td>
            <td>
              {{ mb_strtoupper($pagoscursos->tipo_pagos_nombre) }}
            </td>
            <td>
              {{ mb_strtoupper($pagoscursos->anio) }}           
            </td>
            <td>
              {{ mb_strtoupper($pagoscursos->pago) }}
            </td>
            <td>
              {{ mb_strtoupper($pagoscursos->observaciones) }}
            </td>
            <td>
             <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$pagoscursos -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$pagoscursos -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" onclick="fun_delete('{{$pagoscursos -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
            </td>
            @endforeach
          @endif
          </tr>
        </tbody>
      </table>

    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('cursos_tipos_pagos/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('cursos_tipos_pagos/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agrega pagos de tus cursos </h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos_tipos_pagos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                 <div class="form-group">
                  <label for="last_name">Selecciona curso :</label>
                   <select id="curso" name="curso" required="true"  class="form-control">
                      @foreach($cursos as $c)
                  
                      <option value="{{ $c -> id}}">{{ $c -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
              <div class="form-group">
                  <label for="last_name">Selecciona Tipo de pago :</label>
                   <select id="tipo_pagos" name="tipo_pagos" required="true"  class="form-control">
                      @foreach($tipo_pagos as $r)
                  
                      <option value="{{ $r -> id}}">{{ $r -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
               
                <div class="form-group">
                  <label for="last_name">Selecciona Año:</label>
                  <select  class="form-control" name="anio" id="anio">
                 <option value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>
                  <option value="2027">2027</option>
                </select>
                </div>

                 <div class="form-group">
                  <label for="last_name">Cantidad de pago en Q.:</label>
                  <input type="text" class="form-control" id="pago" name="pago" required="true">
                  
                </div>

                 <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <textarea class="form-control" id="observaciones" name="observaciones" required="true">
                  </textarea>
                </div>
              </div>
              
              <button type="submit" class="btn btn-default"><span class="fa fa-plus"></span>Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->

    <!-- Add Modal Curso start -->
    <div class="modal fade" id="addModalcurso" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Curso</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Descripcion:</label>
                  <input type="text" class="form-control" id="descripcion" name="descripcion" required="true">
                </div>
                
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->

    <!-- Add Modal Tipos de pagos start -->
    <div class="modal fade" id="addModalTipoPagos" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregue tipo de pago </h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('tipos_pagos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" title="Agregue el nombre del pago ej: pago enero" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Tipo de pago:</label>
                
                  <select class="form-control" title="Ejemplo: pago mensual, bimestral, semestral o anual" id="tipo_de_pago" name="tipo_de_pago">
                    <option value="MENSUAL">MENSUAL</option>
                    <option value="BIMESTRAL">BIMESTRAL</option>
                    <option value="TRIMESTRAL">TRIMESTRAL</option>
                    <option value="SEMESTRAL">ANUAL</option>
                    <option value="PAGO UNICO">PAGO UNICO</option>
                    <option value="OTRO">OTRO</option>
                  </select>
                </div>
              
               
                <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <textarea class="form-control" id="Observaciones" name="tipo_de_pago" required="true">
                  </textarea>
                </div>
              </div>
              
              <button type="submit" class="btn btn-default"><span class="fa fa-plus"></span>Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
 
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Curso : </b><span id="view_curso" class="text-success"></span></p>
            <p><b>Tipo de pago : </b><span id="view_tipo_de_pago" class="text-success"></span></p>
            <p><b>Año : </b><span id="view_anio" class="text-success"></span></p>
            <p><b>Pago : </b><span id="view_pago" class="text-success"></span></p>

            <p><b>Observaciones : </b><span id="view_observaciones" class="text-success"></span></p>
            <p><b>Fecha de creación : </b><span id="view_fecha_created" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos_tipos_pagos/update') }}" method="post">
              {{ csrf_field() }}
 <div class="form-group">
                 <div class="form-group">
                  <label for="last_name">Selecciona curso :</label>
                   <select id="edit_curso" name="edit_curso" required="true"  class="form-control">
                      @foreach($cursos as $c)
                  
                      <option value="{{ $c -> id}}">{{ $c -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
              <div class="form-group">
                  <label for="last_name">Selecciona Tipo de pago :</label>
                   <select id="edit_tipo_de_pago" name="edit_tipo_de_pago" required="true"  class="form-control">
                      @foreach($tipo_pagos as $r)
                  
                      <option value="{{ $r -> id}}">{{ $r -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
               
                <div class="form-group">
                  <label for="last_name">Selecciona Año:</label>
                  <select  class="form-control" name="edit_anio" id="edit_anio">
                 <option value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>
                  <option value="2027">2027</option>
                </select>
                </div>

                 <div class="form-group">
                  <label for="last_name">Cantidad de pago en Q:</label>
                  
                   <textarea class="form-control" id="edit_pago" name="edit_pago" required="true">
                  </textarea>
                  
                </div>

                 <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <textarea class="form-control" id="edit_observaciones" name="edit_observaciones" required="true">
                  </textarea>
                </div>
              </div>
              

              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script>
    
    
  </script>
  <script type="text/javascript">

    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result.id);
          $("#view_curso").text(result.curso_nombre);
          $("#view_tipo_de_pago").text(result.tipo_pagos_nombre);
          $("#view_anio").text(result.anio);
          $("#view_pago").text(result.pago);
          $("#view_observaciones").text(result.observaciones);
          $("#view_fecha_created").text(result.fecha_created);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result.id);
          console.log(result.cursos);
          console.log(result.tipo_pagos);
          console.log(result.anio);
          $("#edit_id").val(result.id);
          $("#edit_curso").val(result.cursos);
          $("#edit_curso").select(result.cursos);
          $("#edit_tipo_de_pago").select(result.tipo_pagos);
          $("#edit_tipo_de_pago").val(result.tipo_pagos);
          $("#edit_anio").select(result.anio);
          $("#edit_anio").val(result.anio);
          $("#edit_pago").text(result.pago);
          $("#edit_observaciones").text(result.observaciones);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Are you sure want to delete??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>
</div>


@endsection