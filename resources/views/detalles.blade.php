

@extends('layouts.app')







@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">

    <div class="row">

    <div class="col-md-3">

     <!-- Sidebar -->

    @include('menus.menuadmin')

    </div>

        <div class="col-md-8 ">

              <h1 class="page-header text-center"> <span class="fa fa-money"></span> <span class="fa fa-search"></span>  DETALLES DE PAGOS - {{$nombres}}  {{$apellidos}}</h1>

              

           



            @include('partials.messages')

            <br>

            @if(count($detalles_pago) == 0)

              <p class="text-info">

                No se han registrado pagos aun.

              </p>

            @else

                <div class="table-responsive">

                  <table class="table table-hover table-bordered">

                    <thead>

                      <tr>

                        <th>

                          CODIGO TRANSACCION

                        </th>

                        <th>

                          TIPO DE PAGO 

                        </th>

                        <th>

                          TOTAL 

                        </th>

                        <th>

                          FECHA DE PAGO

                        </th>

                       

                      </tr>

                    </thead>

                    <tbody id="datosAlumnos">

                      @foreach($detalles_pago as $key => $detalles)

                        <tr>

                          <td>

                            {{ $detalles->id }}

                          </td>

                        @if($detalles->mes==1)

                        <td> ENERO </td>

                        @elseif($detalles->mes==2)

                        <td> FEBRERO </td>

                        @elseif($detalles->mes==3)

                        <td> MARZO </td>

                        @elseif($detalles->mes==4)

                        <td> ABRIL </td>

                        @elseif($detalles->mes==5)

                        <td> MAYO </td>

                        @elseif($detalles->mes==6)

                        <td> JUNIO </td>

                        @elseif($detalles->mes==7)

                        <td> JULIO </td>

                        @elseif($detalles->mes==8)

                        <td> AGOSTO </td>

                        @elseif($detalles->mes==9)

                        <td> SEPTIEMBRE </td>

                        @elseif($detalles->mes==10)

                        <td> OCTUBRE </td>

                        @elseif($detalles->mes==11)

                        <td> NOVIEMBRE </td>

                        @elseif($detalles->mes==12)

                        <td> DICIEMBRE </td>

                        @endif

                                     <td>

                            {{ mb_strtoupper($detalles->subtotal) }}

                          </td>

                          <td>

                            {{ mb_strtoupper($detalles->fecha_create) }}

                          </td>

                        </tr>

                      @endforeach

                    </tbody>

                  </table>



                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}



                </div>



            @endif







          </div>



          <!-- /.col-lg-12 -->



      </div>







  </div>




@endsection



