 <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>
                          NO
                        </th>
                        <th>
                          NOMBRE 
                        </th>
                        <th>
                          DESCRIPCION DEL CURSO 
                        </th>
                        <th>
                          PRECIO DEL CURSO (Q.)
                        </th>
                        <th>
                          CREACION
                        </th>
                      </tr>
                    </thead>
                    <tbody id="datosAlumnos">
                      @foreach($cursos as $key => $item)
                        <tr>
                          <td>
                            {{ $item->id }}
                          </td>
                          <td>
                            {{ mb_strtoupper($item->nombre) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($item->descripcion) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($item->precio_curso) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($item->fecha_create) }}
                          </td>
                          <td>
                            {!!link_to_route('cursos.edit', $title = 'Editar', $parameters = $item->id, $attributes = ['class'=>'btn btn-warning'])!!}

                          </td>


                          <td>
                            {!!link_to_route('cursos.show', $title = 'Eliminar', $parameters = $item->id, $attributes = ['class'=>'btn btn-danger'])!!}

                          </td>

                          <td>

                            
                          </td>


                        </tr>

                      @endforeach

                    </tbody>

                  </table>