
@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />


<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">

             

              @include('partials.messages')
              
    <div class="panel panel-default">
            @include('partials.messages')
                <div class="panel-heading">TUS CURSOS
                </div>
    <div class="panel-body">
    <h2>Administra tus cursos</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
   
    <table class="table table-bordered">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> nombre}}</td>
          <td>{{$x -> descripcion}}</td>
          <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id_curso}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id_curso}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" onclick="fun_delete('{{$x -> id_curso}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('cursos/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('cursos/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Curso</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Descripcion:</label>
                  <input type="text" class="form-control" id="descripcion" name="descripcion" required="true">
                </div>
                
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalle de curso</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Descripcion : </b><span id="view_descripcion" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Descripcion :</label>
                  <input type="text" class="form-control" id="edit_descripcion" name="edit_descripcion">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Update</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>
  <script type="text/javascript">
    function fun_view(id)
    {
      //alert(id);
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          //alert(result)

          $("#view_nombre").text(result.nombre);
          $("#view_descripcion").text(result.descripcion);
        },
        error: function (request, status, error) {
                  alert(request.responseText);
              }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id_curso);
          $("#edit_nombre").val(result.nombre);
          $("#edit_descripcion").val(result.descripcion);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desa eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        alert(id);
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          },
          error: function (request, status, error) {
                  alert(request.responseText);
              }
        });
      }
      else{
        return false;
      }
    }
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

