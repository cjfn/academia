

  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="{{ url('/home') }}"><span class="fa fa-home"></span> Principal</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
      
        <div class="panel-footer" title="administra los usuarios que pueden ingresar a la plataforma"><a href="{{ url('/usuario') }}"><span class="fa fa-users"></span> Usuarios</a></div>
        <div onfocus="showMe('onFocusthis')" id="onFocusthis" class="panel-footer" onclick="fn_muestra('alumno_cursos')" title="Esta seccion permitira gestionar alumnos, cursos y asignarlos">
              <strong><span class="fa fa-plus"></span> <span class="fa fa-users"></span><span class="fa fa-book"></span> Alumnos y Cursos</strong>
        </div>
        <div id="alumno_cursos" class="panel">
        <div class="panel-footer" title="Inscribe y administra los alumnos de tu institución"><a href="{{ url('/alumnos') }}"><span class="fa fa-graduation-cap"></span> Alumnos</a></div>

        <div class="panel-footer" title="administra los cursos que se impartiran en todas las jornadas de tu institución"><a href="{{ url('/cursos') }}"><span class="fa fa-book"></span> Cursos</a></div>

        <div class="panel-footer" title="Agrega nuevos Establecimeinto para agregarlos a tus  alumnos inscritos y llevar un mejor control de tus alumnos"><a href="{{ url('/establecimiento') }}"></span> <span class="fa fa-home"></span><span class="fa fa-graduation-cap"></span> Establecimientos</a></div>

        <div class="panel-footer" title="Administra los salones disponibles y el limite máximo de alumnos por salon"><a href="{{ url('/salon') }}"><span class="fa fa-cogs"></span><span class="fa fa-book"></span> salon</a></div>

        <div class="panel-footer" title="Asigna Curos a los alumnos inscritos o actualiza(reinscribir) alumno"><a href="{{ url('/alumnos_cursos') }}"><span class="fa fa-book"></span> <span class="fa fa-plus"></span><span class="fa fa-graduation-cap"></span> Cursos de  Alumnos</a></div>
      </div>

        <div class="panel-footer" title="Crea pagos de los alumnos inscritos en tu institución"><a href="{{ url('/pagos') }}"><span class="fa fa-plus"></span><span class="fa fa-money"></span> Pagos</a></div>

        <div class="panel-footer" title="Adminsitra los tipos de pagos que se realizaran en tu institución, ejemplo: pago de enero"><a href="{{ url('/tipos_pagos') }}"><span class="fa fa-cogs"></span><span class="fa fa-money"></span> Tipos de pagos</a></div>


         <div class="panel-footer" title="Crea pagos de los alumnos inscritos en tu institución" data-toggle="collapse" data-target="#reportes">
          <a title="Ve Reportes en formato pdf que te ayudara a tomar mejores deciciones para ti y tu institucion">
           <span class=" fa fa-plus"></span><span class="fa fa-file"></span> Reportes
         </a>
         </div>
         <center>
        
        <div id="reportes" class="collapse">
          <div class="panel-footer" title="Reporte de todos los alumnos" data-toggle="collapse">
          <span class="fa fa-graduation-cap"></span>
         <input type="submit" value="Alumnos inscritos" class="btn btn-success" onclick = "location='/pdf/'"/>
       </div>

          
        </div>
      </center>
      </div>
    </div>
  </div>
