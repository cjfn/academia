{!! Form::open(array('url'=>'alumnos_cursos','method'=>'GET','autocomplete'=>'off','role'=>'search')) !!}
<div class="form-group">
<br/>
  <div class="input-group">
    
    <input type="text" class="form-control" name="searchText" placeholder="Buscar alumnos por nombre, apellido, año ..." value="{{$searchText}}">

    <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

    <button type="submit" class="btn btn-info"><div class="fa fa-search"></div>Buscar</button>
    </span>
  </div>
</div>

{{Form::close()}}