@extends('layouts.app')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">
        <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8">
              <h1 class="page-header text-center">ADMINISTRAR CURSOS DE ALUMNOS </h1>
              </div>
              <div class="col-xs-12 col-md-2">          
        </div>
        </div>
        </div>
            <center>
            @include('partials.messages')
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
           
            @if(count($alumnos) == 0)
             <p class="text-info">
                No se han registrado alumnos aun.
              </p>
            @else 
                     
             <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">POR ALUMNO</a></li>
                <li><a data-toggle="tab" href="#menu1">POR CURSO</a></li>

              </ul>
              <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                @include('alumno_cursos.search')               
                   <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>
                            DATOS DEL ALUMNO 
                          </th>
                           <th>
                            FECHA INSCRIPCION
                          </th>
                            <th>
                            OPCIONES 
                          </th>
                        </tr>
                      </thead>
                     <tbody>
                        @foreach($alumno as $key => $rol)
                          <tr>                      
                            <td>
                               {{ mb_strtoupper($rol->apellido) }} {{ mb_strtoupper($rol->nombre) }} {{ mb_strtoupper($rol->curso) }}
                           </td>   
                             <td>
                               {{ mb_strtoupper($rol->fecha_create) }} 
                           </td>                        
                           <td>  
                           <!--                           
                              <a href="/cursos_de_alumnos/?a={{$rol->id}}" title="Click aqui para ver mis notas y puntos asignados para este curso" class="btn btn-default"><div class="fa fa-trophy" ></div><span class="fa fa-eye"></span> Ver cursos  </a>
                              
                               <a href="/cursos_de_alumnos/create/?a={{$rol->id}}&nombre={{$rol->nombre}}&apellido={{$rol->apellido}}" title="Click aqui para ver mis notas y puntos asignados para este curso" class="btn btn-success"><span class="fa fa-plus" ></span> Agregar cursos  </a>
                             -->
                              <button class="btn btn-default" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$rol -> id}}')"><span class="fa fa-eye"></span>Ver cursos</button>
                               <button class="btn btn-success" data-toggle="modal" data-target="#addModal" onclick="fun_add('{{$rol -> id}}','{{$rol -> nombre}}','{{$rol -> apellido}}')"><span class="fa fa-plus"></span>Agregar</button>
              
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>
                          CURSO 
                        </th>
                          <th>
                          OPCIONES 
                        </th>
                      </tr>
                    </thead>
                    <tbody id="datosAlumnos">
                      @foreach($alumnos as $key => $rol)
                        <tr>
                          <th>
                            {{ mb_strtoupper($rol->nombre) }}
                          </th>
                         <td>
                         <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('alumnos_cursos/delete')}}">
                       
                           <a href="/cursos_alumnos/?a={{$rol->curso}}&curso={{$rol->nombre}}" title="Click aqui para ver mis notas y puntos asignados para este curso" class="btn btn-success"><div class="fa fa-trophy" ></div><span class="fa fa-eye"></span> Ver estudiantes inscritos  total({{ mb_strtoupper($rol->total) }}) </a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
            <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('alumnos_cursos/view')}}">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
            @endif
          </div>
          <!-- /.col-lg-12 -->
          </div>
           <!-- View Modal start -->
            <div class="modal fade" id="viewModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cursos de <span id="cursoDe"></span></h4>
                  </div>
                  <div class="modal-body">
                   <table id="tblCursos">
                     <tr>
                            <td class="border" style="line-height:12pt;">CURSO</td>
                            <td class="border" style="line-height:12pt;">DESCRIPCION</td>
                            <td class="border" style="line-height:12pt;">HORA INICIO</td>
                            <td class="border" style="line-height:12pt;">HORA FIN</td>
                            <td class="border" style="line-height:12pt;">FECHA INSCRIPION</td>
                      </tr >
                   </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"></button>
                  </div>
                </div>
                
              </div>
            </div>
            <!-- view modal ends -->
         

           <!-- Add Modal start -->
            <div class="modal fade" id="addModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Curso</h4>
                  </div>
                  <div class="modal-body">
                    <form action="{{ url('addcursosAlumnos') }}" method="post">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <div class="form-group">
                          <label for="first_name">Nombre:</label>
                          <input type="text" class="form-control" id="nombres" name="nombres" required="true">
                        </div>
                        <div class="form-group">
                          <label for="last_name">Curso:</label>
                           <select id="cursos" name="cursos" required="true"  class="form-control">
                                      @foreach($cursos as $c)
                                  
                                      <option value="{{ $c -> id_curso}}">{{ $c -> nombre}} </option>
                                  
                                  @endforeach
                              </select>
                          
                        </div>
                        <div class="form-group">
                          <label for="email">Hora inicio:</label>
                           <select  class="form-control" name="hora_inicio" id="hora_inicio">
                               <option value="08:00:00">8:00 am</option>
                               <option value="09:00:00">9:00 am</option>
                               <option value="10:00:00">10:00 am</option>
                               <option value="11:00:00">11:00 am</option>
                               <option value="12:00:00">12:00 am</option>
                               <option value="13:00:00">1:00 pm</option>
                               <option value="14:00:00">2:00 pm</option>
                               <option value="15:00:00">3:00 pm</option>
                               <option value="16:00:00">4:00 pm</option>
                               <option value="17:00:00">5:00 pm</option>
                               <option value="18:00:00">6:00 pm</option>
                              </select>
                      </div>
                        <div class="form-group">
                          <label for="email">Hora fin:</label>
                           <select  class="form-control" name="hora_fin" id="hora_fin">
                               <option value="08:00:00">8:00 am</option>
                               <option value="09:00:00">9:00 am</option>
                               <option value="10:00:00">10:00 am</option>
                               <option value="11:00:00">11:00 am</option>
                               <option value="12:00:00">12:00 am</option>
                               <option value="13:00:00">1:00 pm</option>
                               <option value="14:00:00">2:00 pm</option>
                               <option value="15:00:00">3:00 pm</option>
                               <option value="16:00:00">4:00 pm</option>
                               <option value="17:00:00">5:00 pm</option>
                               <option value="18:00:00">6:00 pm</option>
                              </select>
                      </div>
                      <input type="hidden" id="id" name="id">
                      <button type="submit" class="btn btn-default">Agregar</button>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
                
              </div>
            </div>
            <!-- add code ends -->
            <script type="text/javascript">
              function fun_add($id, $nombre, $apellido)
              {
                var id = $id;
                var nombre = $nombre;
                var apellido = $apellido;
                //alert($id,$nombre,$apellido);
                //alert(nombre);
                var nombres =   $nombre+' '+$apellido;
                $("#nombres").val(nombres);
                $("#id").val($id);
              }

              function fun_view(id)
              {
                //alert(id);
                var view_url = $("#hidden_view").val();
                //alert(view_url);
                $.ajax({
                  url: view_url,
                  type:"GET", 
                  data: {"id":id}, 
                  success: function(response){
                    console.log(response);
                    var i;
                    $("#tblCursos tr").remove(); 
                    $("#cursoDe").text(response[0].nombre_alumno);
                    for (i = 0; i < response.length; i++) {
                     // alert(result[i].nombre);
                     if(i==0)
                     {
                        $('#tblCursos > tbody:last-child').append('<tr>' +
                            '<td class="border" style="line-height:12pt;">CURSO</td>' +
                            '<td class="border" style="line-height:12pt;">DESCRIPCION</td>' +
                            '<td class="border" style="line-height:12pt;">HORA INICIO</td>' +
                            '<td class="border" style="line-height:12pt;">HORA FIN</td>' +
                            '<td class="border" style="line-height:12pt;">FECHA INSCRIPION</td>' +
                      '</tr >' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].nombre + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].descripcion + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].hora_inicio + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].hora_fin + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].fecha_create + '</td>' +
                          
                          '</tr >');


                     }
                     else
                     {
                        $('#tblCursos > tbody:last-child').append('<tr>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].nombre + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].descripcion + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].hora_inicio + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].hora_fin + '</td>' +
                          '<td class="border" style="line-height:12pt;"><!--D-->' + response[i].fecha_create + '</td>' +
                          
                          '</tr >');


                     }
                     
                       
                    } 
                  },
                  error: function (request, status, error) {
                  alert(request.responseText);
              }
                   
                });

              }

            </script>




@endsection



