




@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">

    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')

   

    </div>
        <div class="col-md-8">
            <h1 class="page-header text-center">NUEVO ALUMNO </h1>
        </div>
        <div class="col-md-8">
        
        <div class="panel panel-default">
        <div class="panel panel-default"> 
        {!!Form::open(['route'=>'alumnos_cursos.store','method'=>'POST'])!!}
                        <div class="from-group">
            {!!Form::label('Alumno *')!!}
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Nombre','required' => 'required'])!!}
            </div>


            <div class="from-group">
            
            </div>

            <div class="from-group">
            {!!Form::label('Hora Inicio *')!!}
            {!!Form::date('hora_inicio', ['class'=>'form-control'])!!}
            <a href="">
            </div>
 
            <div class="from-group">
            {!!Form::label('Hora Fin *')!!}
            {!!Form::date('hora_fin', ['class'=>'form-control'])!!}

            </div>
            <center>

            {!!Form::submit('Crear nuevo',['name'=>'grabar','id'=>'grabar','class'=>'btn btn-success fa fa-plus'])!!}

            </center>
            <br>
        {!!Form::close()!!}
        </div>

        <div class="col-md-4">
        </div>
        </div>
        </div>

    </div>
</div>
@endsection