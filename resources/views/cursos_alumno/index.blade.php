

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
     
    </div>
        <div class="col-md-8 panel panel-default">
        <div class="panel-body">
        @if($alumno==null)
        <p> no hay registros </p>
        @else
         <H2 class="text-center"> ALUMNOS DEL CURSO DE {{ $curso }}
          <br/>
          <a href="../pdf" class="btn btn-danger"><span class="fa fa-file-pdf-o"></span>Reporte  de todos los alumnos de todas las jornadas Pdf</a>
        </H2>
        <CENTER>
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><span class="fa fa-plus"></span>PLAN DIARIO</button>


        </CENTER>
        <div id="demo" class="collapse">
        <div class="table-responsive">

        <a href="../alumnos_jornadas/a={{$curso}}" class="btn btn-danger"><span class="fa fa-file-pdf-o"></span>Reporte  de todos los alumnos de todas las jornadas Pdf</a>
        <table class="table table-bordered table-hover">
        <thead>
          <tr>
             <td><strong>cod</strong></td>
            <td><strong>apellidos y nombres </strong></td>
            <td><strong>hora de inicio</strong></td>
            <td><strong>hora de fin</strong></td>
          </tr>
        </thead> 
        <tbody>
           @foreach ($alumno as $key=>$actividad)




                  <tr>
                    <td>
                      <h5 class=" glyphicon glyphicon-edit"> {{ $key+1 }}</h5>
                    </td>              
                   <td>
                      {{ mb_strtoupper($actividad->apellido) }}
                   
                      {{ mb_strtoupper($actividad->nombre) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($actividad->hora_inicio) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($actividad->hora_fin ) }}
                    </td>
                   
                  </tr>
                @endforeach
              </tbody>
            </table>
            @endif
          </div>
      </div>   

         <CENTER>
        <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo2"><span class="fa fa-plus"></span>PLAN FIN DE SEMANA</button>
        </CENTER>
        <div id="demo2" class="collapse">
        <div class="table-responsive">
        <table class="table table-bordered table-hover">
        <thead>
          <tr>
             <td><strong>cod</strong></td>
            <td><strong>apellidos y nombres </strong></td>
            <td><strong>hora de inicio</strong></td>
            <td><strong>hora de fin</strong></td>
          </tr>
        </thead> 
        <tbody>
           @foreach ($alumno_finde as $key=>$actividad)

                  <tr>
                    <td>
                      <h5 class=" glyphicon glyphicon-edit"> {{ $key+1 }}</h5>
                    </td>              
                   <td>
                      {{ mb_strtoupper($actividad->apellido) }}
                      {{ mb_strtoupper($actividad->nombre) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($actividad->hora_inicio) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($actividad->hora_fin) }}
                    </td>            
                  </tr>
                @endforeach
              </tbody>
            </table>
           
          </div>
      </div>   
    </div>
</div>
@endsection