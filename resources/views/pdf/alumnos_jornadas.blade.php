
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <style>
      table, td, th {    
    border: 1px solid black;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
    </style>





  </head>
  <body>

          <CENTER>
          <h2>ALUMNOS CON CURSOS <STRONG>ACADEMIA DE COMPUTACION VELVETH </STRONG></h2>
          <H5>FECHA: - TOTAL DE ALUMNOS {{$data_total}}</H5>
          
      <table border="1">
        <thead>
          <tr>
            <th class="">ALUMNO</th>
            <th class="">TELEFONO</th>
            <th class="">DIRECCION</th>
            <th class="">GRADO</th>
            <th class="">DEPARTAMENTO</th>
            <th class="">PLAN</th>
             <th class="">CURSO</th>
             <th class="">HORA INICIO</th>
             <th class="">HORA FIN</th>
      

          </tr>
        </thead>
        <tbody>
          
             @foreach($data as $key => $dat)
             <tr>
            <td class="">{{ $dat->nombre }}  {{ $dat->apellido }}</td>
            <td class="">{{ $dat->telefono }}</td>
            <td class="">{{ $dat->direccion }}</td>
            <td class="">{{ $dat->grado }}</td>
            <td class="">{{ $dat->nombre_departamento }}</td>
            <td class="">{{ $dat->jornada }}</td>
            <td class="">{{ $dat->curso }}</td>
            <td class="">{{ $dat->hora_inicio }}</td>
            <td class="">{{ $dat->hora_fin }}</td>
            </tr>
            @endforeach

          

        </tbody>
       
      </table>
  </body>
</html>
