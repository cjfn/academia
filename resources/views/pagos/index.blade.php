

@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">

             

            @include('partials.messages')
            

    <div class="panel panel-default">
            @include('partials.messages')
                <div class="panel-heading">PAGOS DE TUS ALUMNOS
                </div>
    <div class="panel-body">
    <h2 class="text-center">Crea y administra los pagos </h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
   
    <table class="table table-bordered" id="myTable">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span><span class="fa fa-money"></span>Hacer pago</button>
    @include('pagos/search')
    </center>
      <thead>
        <tr>
          <strong>
          <th>No.</th>
          <th>nombre</th>
          <th>Curso</th>
          <th>Tipo de pago</th>
          <th>Ultimo pago Q.</th>
          <th>fecha de ultimo pago</th>
          <th>opciones</th>
          </strong>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> id}}</td>
          <td>{{$x -> apellido}}  {{$x -> nombre}}</td>
          <td> {{$x -> nombre_curso}}</td>
          <td><strong>{{$x -> nombre_tipo_pagos}}</strong></td>
          <td><strong>Q.{{$x -> total}}</strong></td>
          <td>{{$x -> fecha_pago}}</td>
          <td>

            

             <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span> Detalle de pago</button>
            
             <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('pagos/view')}}">
             <input type="hidden" name="hidden_alumnos_view" id="hidden_alumnos_view" value="{{url('findAlumnosNombres')}}">
             <input type="hidden" name="hidden_alumnos_cursos_view" id="hidden_alumnos_cursos_view" value="{{url('findAlumnosCursos')}}">
             
          </td>
        </tr>
       @endforeach
      </tbody>



    </table>
      <div class="text-center">
            {!! $data->links() !!}
      </div>
   
    <!-- modal al seleccionar alumno-->
    <div class="modal fade" id="addNewModal" role="dialog">
      <div class="modal-dialog">
      
         <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar Pago</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('pagos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
             
                <div class="form-group">
                  <label for="last_name">Tipo de pago :</label>
                   <select id="tipo_pagos" name="tipo_pagos" required="true"  class="form-control">
                      @foreach($tipo_pagos as $r)
                  
                      <option value="{{ $r -> id}}">{{ $r -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
                <div class="form-group">
                  <label for="subtotal">Total:</label>
                  <input type="number" class="form-control" id="total" name="total" step="any" required="true">
                </div>
              
                 </div>
               
              </div>
              <center>
              <button type="submit" class="btn btn-success"><span class="fa fa-money"></span>Hacer Pago</button>
              </center>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->

    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar Pago</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('pagos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  
                   <select class="itemName form-control" style="width:500px;" id="alumno" name="alumno" required="true" onchange="myFunction()"></select>
                   <p id="demo"></p>
                </div>
                <div class="form-group">
                  <label for="last_name">Tipo de pago :</label>
                   <select id="tipo_pagos" name="tipo_pagos" required="true"  class="form-control">
                      @foreach($tipo_pagos as $r)
                      <option value="{{ $r -> id}}"> {{ $r -> nombre}}   TOTAL: {{ $r -> pago}} </option>
                  @endforeach
                   </select>
                </div>
               
                <div class="form-group">
                  <label for="total">Año:</label>
                 <input type="number" min="1900" max="2099" step="1" name="año" id="año" class="form-control" />
                </div>
               <div class="form-group">
                <label for="pago">Forma de pago:</label>
                 <div class="from-group">
           
                <select  class="form-control" name="forma_pago" id="forma_pago">
                 <option value="Efectivo">Efectivo</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Deposito">Deposito</option>
                  <option value="Otro">Otro</option>
                </select>
                </div>
                </div>
               
              </div>
              <center>
              <button type="submit" class="btn btn-success"><span class="fa fa-money"></span>Hacer Pago</button>
              </center>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title ">DETALLES DE PAGOS</h4>
          </div>
          <div class="modal-body">
            <p><b>Alumno : </b><span id="view_nombre" class="text-success"></span><span id="view_apellido" class="text-success"></span></p>
            <p><b>Tipo de pago : </b><span id="view_nombre_tipo_pagos" class="text-success"></span></p>
            <p><b>total : </b><span id="view_pago" class="text-success"></span></p>
            <p><b>fecha pago : </b><span id="view_fecha_create" class="text-success"></span></p>
           
           
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuario/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_name" name="edit_name">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Email :</label>
                  <input type="email" class="form-control" id="edit_email" name="edit_email">
                </div>

                <div class="form-group">
                  <label for="edit_descripcion">Contraseña :</label>
                  <input type="text" class="form-control" id="edit_password" name="edit_password">
                </div>
                <label for="edit_tipo_usuario">Tipo de Usuario:</label>
                <select id="tipo_usuario" name="tipo_usuario" required="true"  class="form-control">
                
                </select>
              </div>
              
              <button type="submit" class="btn btn-default">Update</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>



<script type="text/javascript">
$(document).ready(function () {
    var currentTime = new Date()
      // returns the month (from 0 to 11)
      var month = currentTime.getMonth() + 1
      // returns the day of the month (from 1 to 31)
      var day = currentTime.getDate()
      // returns the year (four digits)
      var year = currentTime.getFullYear()

      // write output MM/dd/yyyy 
      $("#año").val(year);


});

//on select change
    $("select[name='id_alumno']").change(function(){
      var id = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('select-ajax') ?>",
          method: 'POST',
          data: {id:id, _token:token},
          success: function(data) {
            $("select[name='id'").html('');
            $("select[name='id'").html(data.options);
            alert("select[name='id_alumno'");
          },
           error: function (request, status, error) {
                  alert(request.responseText);
              }
      });
  });



//SELECT DE ALUMNO
      $('.itemName').select2({
        placeholder: 'Seleccione un alumno',
        ajax: {
          url: '/select2-autocomplete-ajax',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {

                        text: item.nombre+' '+item.apellido,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

  $("#alumno").change(function(){
    
});
  //FUNCION PARA GARGAR EL ID DEL ALUMNO



  function myFunction()
          {
                        var x = document.getElementById("alumno").value;
                       
                          //var alumnos_url = $("#hidden_alumnos_view").val();
                          var alumnos_cursos_url = "findAlumnosCursos/?id="+x;
                          
                          $.ajax({

                            url:alumnos_cursos_url,
                            type:'GET',
                            data:{'id':x},
                            success:function(result){
                              console.log(result);

                             
                            },
                            error: function (request, status, error) {
                                alert(request.responseText);
                              }
                          });
                    }





  
// CRUD

    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          //alert(result);
          $("#view_nombre").text(result[0].nombre);
          $("#view_apellido").text(result[0].apellido);
          $("#view_nombre_tipo_pagos").text(result[0].nombre_tipo_pagos);
          $("#view_pago").text(result[0].pago);
          $("#view_fecha_create").text(result[0].fecha_create);
        },
         error: function (request, status, error) {
                  alert(request.responseText);
              }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_descripcion").val(result.descripcion);
          $("#edit_precio_curso").val(result.precio_curso);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Are you sure want to delete??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>




   


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection
