
@extends('layouts.app')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')
    </div>
        <div class="col-md-8 ">
              <h1 class="page-header text-center"> <span class="fa fa-money"></span> <span class="fa fa-search"></span>  DETALLES DE PAGOS - {{$nombres}}  {{$apellidos}}</h1>
              
           

            @include('partials.messages')
            <br>
            @if(count($detalles_pago) == 0)
              <p class="text-info">
                No se han registrado pagos aun.
              </p>
            @else
                <div class="table-responsive">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>
                         COD
                        </th>
                        <th>
                          CURSO
                        </th>
                        <th>
                          TOTAL 
                        </th>
                        <th>
                          TIPO DE PAGO
                        </th>
                        <th>
                          FECHA DE PAGO
                        </th>

                         <th>
                          DETALLES
                        </th>
                       
                      </tr>
                    </thead>
                    <tbody id="datosAlumnos">
                      @foreach($detalles_pago as $key => $detalles)
                        <tr>
                          <td>
                            {{ $detalles->id_alumno_pagos }}
                          </td>
                           <td>
                            {{ mb_strtoupper($detalles->nombre) }}
                          </td>
                           <td>
                            {{ mb_strtoupper($detalles->tipo_pago) }}
                          </td>
                          <td>
                            Q. {{ mb_strtoupper($detalles->total) }}
                          </td>
                          <td>
                            {{ mb_strtoupper($detalles->fecha_create) }}
                          </td>
                           <td>
                             <button class="btn btn-warning" data-toggle="modal" data-target="#editarModal" onclick="fun_edit('{{$detalles -> id}}')">Edit</button>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>

                   <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('pago/detalles/view')}}">

                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}

                </div>

                         <!-- Edit Modal start -->
    <div class="modal fade" id="editarModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_alumno">Alumnos:</label>
                  <input type="text" class="form-control" id="edit_alumno" name="edit_alumno">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Mes :</label>
                  <input type="text" class="form-control" id="edit_mes" name="edit_mes">
                </div>
                <label for="edit_">Pago :</label>
                <input type="number" class="form-control" id="edit_pago" name="edit_pago">
              </div>
              
              <button type="submit" class="btn btn-default">Update</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->


            @endif

    


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>
  <script type="text/javascript">
    
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_alumno").val(result.alumno);
          $("#edit_mes").val(result.mes);
          $("#edit_pago").val(result.subtotal);
        }
      });
    }
 
  </script>

  @endsection
