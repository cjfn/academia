

@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->
    @include('menus.menuadmin')


    </div>
        <div class="col-md-8 ">
        <h3>Agregar Pago </h3>
        {!!Form::model($alumno, ['route'=>['pagos.update',$alumno->id],'method'=>'POST'])!!}

            {{ Form::hidden('alumno', $alumno->id, array('id' => 'id')) }}


        	<div class="from-group">
        	{!!Form::label('Nombre')!!}
        	{!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Nombre','readonly'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('Apellido')!!}
        	{!!Form::text('apellido',null,['id'=>'apellido','class'=>'form-control','placeholder'=>'Apellido','readonly'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('clave')!!}
        	{!!Form::number('clave',null,['id'=>'clave','class'=>'form-control','placeholder'=>'clave','readonly'])!!}
        	</div>

        	<div class="from-group">
        	{!!Form::label('Seleccione su pago')!!}
        	 {!!Form::select('tipo_pagos', $tipo_pagos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione el tipo de pago...'])!!}

        	</div>

        	
        <div class="from-group">
            {!!Form::label('Su pago en Q. *')!!}
            {!!Form::number('subtotal',null,['id'=>'subtotal','class'=>'form-control','placeholder'=>'Ingrese cantidad exacta en Q.','required' => 'required'])!!}
            </div>
        

        	{!!Form::submit('Hacer Pago',['name'=>'grabar','id'=>'grabar','content'=>'<span class="fa fa-money"></span>','class'=>'btn btn-success btn-sm m-t-10'])!!}


        {!!Form::close()!!}
        </div>

    </div>
</div>
@endsection