-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-05-2017 a las 16:18:08
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `academia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
`id` int(11) NOT NULL,
  `clave` int(11) DEFAULT NULL,
  `codigo` varchar(30) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `apellido` varchar(60) DEFAULT NULL,
  `departamento` int(11) DEFAULT NULL,
  `municipio` int(11) DEFAULT NULL,
  `establecimiento` int(11) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `ciclo` int(11) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `clave`, `codigo`, `nombre`, `apellido`, `departamento`, `municipio`, `establecimiento`, `telefono`, `direccion`, `fecha_nacimiento`, `ciclo`, `fecha_create`, `fecha_update`, `activo`) VALUES
(8, 20, NULL, 'jose', 'florian', 11, 147, 1, 144574512, 'guatemala', '1990-05-03', NULL, '2017-05-08 14:17:45', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_cursos`
--

CREATE TABLE IF NOT EXISTS `alumno_cursos` (
`id_alumno_curso` int(11) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `curso` int(11) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `jornada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_cursos_notas`
--

CREATE TABLE IF NOT EXISTS `alumno_cursos_notas` (
`id_alumno_cursos_notas` int(11) NOT NULL,
  `alumno_cursos` int(11) DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_pagos`
--

CREATE TABLE IF NOT EXISTS `alumno_pagos` (
`id_alumno_pagos` int(11) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `subtotal` decimal(10,0) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE IF NOT EXISTS `bitacora` (
`id_bitacora` bigint(20) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `pago` int(11) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `usuario` int(11) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
`id_curso` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nombre`, `descripcion`, `fecha_create`, `fecha_update`) VALUES
(1, 'Tecnico Operador', 'curso diplomado', '2017-05-04 08:27:05', '0000-00-00 00:00:00'),
(2, 'Tecnico Programador', 'curso diplomado', '2017-05-04 08:27:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `id_departamento` smallint(6) NOT NULL COMMENT 'Campo para almacenar el identificador unico de cada registro',
  `nombre_departamento` varchar(50) DEFAULT NULL COMMENT 'campo para almacenar el nombre del departamento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar todo los datos de los departamentos de guatemala';

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre_departamento`) VALUES
(1, 'alta verapaz'),
(2, 'baja verapaz'),
(3, 'chimaltenango'),
(4, 'chiquimula'),
(5, 'el progreso'),
(6, 'escuintla'),
(7, 'guatemala'),
(8, 'huehuetenango'),
(9, 'izabal'),
(10, 'jalapa'),
(11, 'jutiapa'),
(12, 'petén'),
(13, 'quetzaltenango'),
(14, 'quiché'),
(15, 'retalhuleu'),
(16, 'sacatepéquez'),
(17, 'san marcos'),
(18, 'santa rosa'),
(19, 'sololá'),
(20, 'suchitepéquez'),
(21, 'totonicapán'),
(22, 'zacapa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establecimiento`
--

CREATE TABLE IF NOT EXISTS `establecimiento` (
`codigo_establecimiento` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `establecimiento`
--

INSERT INTO `establecimiento` (`codigo_establecimiento`, `nombre`, `direccion`) VALUES
(1, 'Colegio San miguel', 'Jutiapa'),
(2, 'Colegio San Cristobal', 'Jutiapa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE IF NOT EXISTS `jornada` (
`id_jornada` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE IF NOT EXISTS `municipio` (
`id_municipio` int(11) NOT NULL,
  `nombre_municipio` varchar(70) DEFAULT NULL COMMENT 'Campo para alamacenar el nombre del municipio',
  `id_departamento` smallint(6) DEFAULT NULL COMMENT 'Campo para almacenar el identificador unico de cada registro'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar todos los nombres de los municipios del país de guatemala.' AUTO_INCREMENT=350 ;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id_municipio`, `nombre_municipio`, `id_departamento`) VALUES
(15, 'cobán', 1),
(16, 'chisec', 1),
(17, 'chahal', 1),
(18, 'fray bartolomé de las casas', 1),
(19, 'lanquín', 1),
(20, 'panzós', 1),
(21, 'tamahú', 1),
(22, 'tucurú', 1),
(23, 'tactic', 1),
(24, 'santa maria cahabón', 1),
(25, 'senahú', 1),
(26, 'san cristóbal verapaz', 1),
(27, 'san juan chamelco', 1),
(28, 'san pedro carchá', 1),
(29, 'santa cruz verapaz', 1),
(30, 'santa catalina la tinta', 1),
(31, 'raxruhá', 1),
(32, 'cubulco', 2),
(33, 'santa cruz el chol', 2),
(34, 'granados', 2),
(35, 'purulhá', 2),
(36, 'rabinal', 2),
(37, 'salamá', 2),
(38, 'san miguel chicaj', 2),
(39, 'san jerónimo', 1),
(40, 'chimaltenango', 3),
(41, 'san jose poaquil', 3),
(42, 'san martin jilotepeque', 3),
(43, 'san juan comalapa', 3),
(44, 'santa apolonia', 3),
(45, 'sanata cruz balanyá', 3),
(46, 'tecpán', 3),
(47, 'patzún', 3),
(48, 'pochuta', 3),
(49, 'patzicía', 3),
(50, 'el tejar', 3),
(51, 'parramos', 3),
(52, 'acatenango', 3),
(53, 'yepocapa', 3),
(54, 'san andres itzapa', 3),
(55, 'zaragoza', 3),
(56, 'chiquimula', 4),
(57, 'camotan', 4),
(58, 'concepción las minas', 4),
(59, 'esquipulas', 4),
(60, 'ipala', 4),
(61, 'jocotan', 4),
(62, 'olopa', 4),
(63, 'quetzaltepeque', 4),
(64, 'san josé la arada', 4),
(65, 'san juan ermita', 4),
(66, 'san jacinto', 4),
(67, 'guastatoya', 5),
(68, 'morazán', 5),
(69, 'el jicaro', 5),
(70, 'san agustín acasaguastlán', 5),
(71, 'san antonio la paz', 5),
(72, 'sanarate', 5),
(73, 'sansare', 5),
(74, 'escuintla', 6),
(75, 'guanagazapa', 6),
(76, 'iztapa', 6),
(77, 'la democracia', 6),
(78, 'la gomera', 6),
(79, 'masagua', 6),
(80, 'nueva concepción', 6),
(81, 'palín', 6),
(82, 'san jośe', 6),
(83, 'san vicente pacaya', 6),
(84, 'santa lucia cotzumalguapa', 6),
(85, 'siquinalá', 6),
(86, 'tiquisate', 6),
(87, 'guatemala', 7),
(88, 'santa catarina pinula', 7),
(89, 'san josé pinula', 7),
(90, 'palencia', 7),
(91, 'chinautla', 7),
(92, 'san pedro ayampuc', 7),
(93, 'mixco', 7),
(94, 'san pedro sacatepéquez', 7),
(95, 'san juan sacatepéquez', 7),
(96, 'chuarrancho', 7),
(97, 'villa nueva', 7),
(98, 'villa canales', 7),
(99, 'amatitlán', 7),
(100, 'fraijanes', 7),
(101, 'san miguel petapa', 7),
(102, 'san raymundo', 7),
(103, 'aguacatán', 8),
(104, 'chiantla', 8),
(105, 'colotenango', 8),
(106, 'concepción huista', 8),
(107, 'cuilco', 8),
(108, 'huehuetenango', 8),
(109, 'jacaltenango', 8),
(110, 'la democracia', 8),
(111, 'la libertad', 8),
(112, 'malacatancito', 8),
(113, 'nentón', 8),
(114, 'san antonio huista', 8),
(115, 'san gaspar ixchil', 8),
(116, 'san ildefonso ixtahuacán', 8),
(117, 'san juan atitán', 8),
(118, 'san juan ixcoy', 8),
(119, 'san mateo ixtatán', 8),
(120, 'san miguel acatán', 8),
(121, 'san pedro necta', 8),
(122, 'san pedro soloma', 8),
(123, 'san rafael la independencia', 8),
(124, 'san rafael petzal', 8),
(125, 'san sebastián coatán', 8),
(126, 'san sebastián huehhuetenango', 8),
(127, 'santa ana huista', 8),
(128, 'santa bárbara', 8),
(129, 'santa cruz barillas', 8),
(130, 'santa eulalia', 8),
(131, 'santiago chimaltenango', 8),
(132, 'tectitán', 8),
(133, 'todos santos cuchumatán', 8),
(134, 'union cantinil', 8),
(135, 'puerto barrios', 9),
(136, 'el estor', 9),
(137, 'livingston', 9),
(138, 'los amates', 9),
(139, 'morales', 9),
(140, 'jalapa', 10),
(141, 'mataquescuintla', 10),
(142, 'monjas', 10),
(143, 'san pedro pinula', 10),
(144, 'san luis jilotepeque', 10),
(145, 'san manuel chaparrón', 10),
(146, 'san carlos alzatate', 10),
(147, 'jutiapa', 11),
(148, 'agua blanca', 11),
(149, 'asunción mita', 11),
(150, 'atescatempa', 11),
(151, 'comapa', 11),
(152, 'conguaco', 11),
(153, 'el adelantado', 11),
(154, 'el progeso', 11),
(155, 'jalpatagua', 11),
(156, 'jerez', 11),
(157, 'moyuta', 11),
(158, 'pasaco', 11),
(159, 'quesada', 11),
(160, 'san jóse acatempa', 11),
(161, 'santa catarina mita', 11),
(162, 'yupiltepeque', 11),
(163, 'zapotitlan', 11),
(164, 'dolores', 12),
(165, 'flores', 12),
(166, 'la libertad', 12),
(167, 'melchor de mencos', 12),
(168, 'poptún', 12),
(169, 'san andres', 12),
(170, 'san benito', 12),
(171, 'san francisco', 12),
(172, 'san josé', 12),
(173, 'san luis', 12),
(174, 'santa ana', 12),
(175, 'sayaxché', 12),
(176, 'quetzaltenango', 13),
(177, 'almolonga', 13),
(178, 'cabricán', 13),
(179, 'cajolá', 13),
(180, 'cantel', 13),
(181, 'coatepeque', 13),
(182, 'colomba', 13),
(183, 'concepción chiquirichapa', 13),
(184, 'el palmar', 13),
(185, 'flores costa cuca', 13),
(186, 'génova', 13),
(187, 'huitán', 13),
(188, 'la esperanza', 13),
(189, 'olintepeque', 13),
(190, 'san juan ostuncalco', 13),
(191, 'palestina del los altos', 13),
(192, 'salcajá', 13),
(193, 'san carlos sija', 13),
(194, 'san francisco la unión', 13),
(195, 'san martín sacatepéquez', 13),
(196, 'san mateo', 13),
(197, 'san miguel sigüilá', 13),
(198, 'sibilia', 13),
(199, 'zunil', 13),
(200, 'santa cruz del quiché', 14),
(201, 'canillá', 14),
(202, 'chajul', 14),
(203, 'chicamán', 14),
(204, 'chiché', 14),
(205, 'chichicastenango', 14),
(206, 'chinique', 14),
(207, 'cunén', 14),
(208, 'ixcán', 14),
(209, 'joyabaj', 14),
(210, 'nebaj', 14),
(211, 'pachalum', 14),
(212, 'patzité', 14),
(213, 'sacapulas', 14),
(214, 'san andrés sajcabajá', 14),
(215, 'san antonio ilotenango', 14),
(216, 'san bartolomé jocotenango', 14),
(217, 'san juan cotzal', 14),
(218, 'san pedro jocopilas', 14),
(219, 'uspantán', 14),
(220, 'zacualpa', 14),
(221, 'retalhuleu', 15),
(222, 'champerico', 15),
(223, 'el asintal', 15),
(224, 'nuevo san carlos', 15),
(225, 'san andrés villa seca', 15),
(226, 'san martín zapotitlan', 15),
(227, 'san felipe', 15),
(228, 'san sebastián', 15),
(229, 'santa cruz muluá', 15),
(230, 'antigua guatemala', 16),
(231, 'alotenango', 16),
(232, 'ciudad vieja', 16),
(233, 'jocotenango', 16),
(234, 'magdalena milpas altas', 16),
(235, 'pastores', 16),
(236, 'san antonio aguas calientes', 16),
(237, 'san bartolomé milpas altas', 16),
(238, 'san lucas sacatepéquez', 16),
(239, 'san miguel dueñas', 16),
(240, 'santa catarina barahona', 16),
(241, 'santa lucia milpas latas', 16),
(242, 'santa maria de jesús', 16),
(243, 'santiago sacatepéquez', 16),
(244, 'santo domingo xenacoj', 16),
(245, 'sumpango', 16),
(246, 'san marcos', 17),
(247, 'ayutla', 17),
(248, 'catarina', 17),
(249, 'comitancillo', 17),
(250, 'concepción tutuapa', 17),
(251, 'el quetzal', 17),
(252, 'el rodeo', 17),
(253, 'el tumbador', 17),
(254, 'esquipulas palo gordo', 17),
(255, 'ixchiguán', 17),
(256, 'la reforma', 17),
(257, 'malacatán', 17),
(258, 'nuevo progreso', 17),
(259, 'ocós', 17),
(260, 'pajapita', 17),
(261, 'río blanco', 17),
(262, 'san antonio sacatepéquez', 17),
(263, 'san cristóbal cucho', 17),
(264, 'san josé ojetenam', 17),
(265, 'lorenzo', 17),
(266, 'san miguel ixtahuacán', 17),
(267, 'san pablo', 17),
(268, 'san pedro sacatépequez', 17),
(269, 'san rafaél pie de la cuesta', 17),
(270, 'sibinal', 17),
(271, 'sipacapa', 17),
(272, 'tacaná', 17),
(273, 'tajumulco', 17),
(274, 'tejutla', 17),
(275, 'la blanca', 17),
(276, 'cuilapa', 18),
(277, 'casillas', 18),
(278, 'chiquimulilla', 18),
(279, 'guazacapán', 18),
(280, 'nueva santa rosa', 18),
(281, 'oratorio', 18),
(282, 'pueblo nuevo viñas', 18),
(283, 'san juan tecuaco', 18),
(284, 'san rafaél las flores', 18),
(285, 'santa cruz naranjo', 18),
(286, 'santa maría ixhuatán', 18),
(287, 'santa rosa de lima', 18),
(288, 'taxisco', 18),
(289, 'barberena', 18),
(290, 'sololá', 19),
(291, 'concepción', 19),
(292, 'nahualá', 19),
(293, 'panajachel', 19),
(294, 'san andres semetabaj', 19),
(295, 'san antonio palopó', 19),
(296, 'san josé chacayá', 19),
(297, 'san juan la laguna', 19),
(298, 'san lucas tolimán', 19),
(299, 'san marcos la laguna', 19),
(300, 'san pablo la laguna', 19),
(301, 'san pedro la laguna', 19),
(302, 'santa catarina ixtahuacan', 19),
(303, 'santa catarina palopó', 19),
(304, 'santa catarina palopó', 19),
(305, 'santa clara la laguna', 19),
(306, 'santa cruz la laguna', 19),
(307, 'santa lucía utatlán', 19),
(308, 'santa maría visitación', 19),
(309, 'santiago atitlán', 19),
(310, 'mazatenango', 20),
(311, 'chicacao', 20),
(312, 'cuyotenengo', 20),
(313, 'patulul', 20),
(314, 'pueblo nuevo', 20),
(315, 'río bravo', 20),
(316, 'samayac', 20),
(317, 'san antonio suchitepéquez', 20),
(318, 'san bernardino', 20),
(319, 'san josé el ídolo', 20),
(320, 'san francisco zapotitlán', 20),
(321, 'san gabriel', 20),
(322, 'san juan bautista', 20),
(323, 'san lorenzo', 20),
(324, 'san miguel panán', 20),
(325, 'san pablo jocopilas', 20),
(326, 'santa bárbara', 20),
(327, 'santo domingo suchitepéquez', 20),
(328, 'santo tomas la unión', 20),
(329, 'zunilito', 20),
(330, 'san jose la maquina', 20),
(331, 'totonicapán', 21),
(332, 'momostenango', 21),
(333, 'san andrés xecul', 21),
(334, 'san bartolo', 21),
(335, 'san cristobal totonicapán', 21),
(336, 'san francisco el alto', 21),
(337, 'santa lucía la reforma', 21),
(338, 'santa maría cuiquimula', 21),
(339, 'cabañas', 22),
(340, 'estanzuela', 22),
(341, 'gualán', 22),
(342, 'huité', 22),
(343, 'la unión', 22),
(344, 'río hondo', 22),
(345, 'san diego', 22),
(346, 'san jorge', 22),
(347, 'teculután', 22),
(348, 'usumatlán', 22),
(349, 'zacapa', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opciones`
--

CREATE TABLE IF NOT EXISTS `opciones` (
`id_opcion` int(11) NOT NULL,
  `nombre_opcion` varchar(50) NOT NULL,
  `contenedor_principal` smallint(6) NOT NULL,
  `ruta_opcion` varchar(50) NOT NULL,
  `estado_opcion` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los nombres de las opciones del sistema' AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_opcion` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los permisos disponibles del sistema.' AUTO_INCREMENT=96 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
`id_permission_role` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `state_permission_role` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para relacionar los permisos con los roles' AUTO_INCREMENT=171 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state_rol` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los roles disponibles en la aplicación' AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `state_rol`) VALUES
(9, 'administrador', 'administrador', 'permisos para todo', '0000-00-00 00:00:00', NULL, 1),
(10, 'docente', 'docente', 'permisos para modificar', '0000-00-00 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
`id_role_user` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las relaciones entre usuarios y roles' AUTO_INCREMENT=340 ;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id_role_user`, `role_id`, `user_id`) VALUES
(339, 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE IF NOT EXISTS `tipo_usuario` (
`id_tipo_usuario` int(11) NOT NULL,
  `tipo_usuario` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) NOT NULL,
  `fecha_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo_usuario`, `tipo_usuario`, `descripcion`, `fecha_create`, `fecha_update`) VALUES
(1, 'administrador', 'permisos a todo', '2017-05-04 10:06:17', '0000-00-00 00:00:00'),
(2, 'profesor', 'permiso a modificar no a crear', '2017-05-04 10:06:17', '0000-00-00 00:00:00'),
(3, 'web_master', 'gran all permises', '2017-05-04 10:06:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo_usuario` int(11) DEFAULT NULL,
  `estado_usuario` int(11) DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `tipo_usuario`, `estado_usuario`) VALUES
(1, 'jose', 'cjfn10101@gmail.com', '$2y$10$ACdymbZfheH5GFHFbnURs.sdMGcNjIOYAoIPIKqytgGwMbiyRqJBq', 'NlGqJo72dFLQ6fsmbo6vXtZRzWK64CW7d6aoX1TWUurefUOHFUY1Z5DIlStm', '2017-05-06 22:46:09', '2017-05-08 09:07:33', 9, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
 ADD PRIMARY KEY (`id`), ADD KEY `departamento` (`departamento`,`municipio`,`establecimiento`);

--
-- Indices de la tabla `alumno_cursos`
--
ALTER TABLE `alumno_cursos`
 ADD PRIMARY KEY (`id_alumno_curso`), ADD KEY `alumno` (`alumno`,`curso`);

--
-- Indices de la tabla `alumno_cursos_notas`
--
ALTER TABLE `alumno_cursos_notas`
 ADD PRIMARY KEY (`id_alumno_cursos_notas`), ADD KEY `alumno_cursos` (`alumno_cursos`);

--
-- Indices de la tabla `alumno_pagos`
--
ALTER TABLE `alumno_pagos`
 ADD PRIMARY KEY (`id_alumno_pagos`), ADD KEY `alumno` (`alumno`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
 ADD PRIMARY KEY (`id_bitacora`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
 ADD PRIMARY KEY (`id_curso`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
 ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `establecimiento`
--
ALTER TABLE `establecimiento`
 ADD PRIMARY KEY (`codigo_establecimiento`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
 ADD PRIMARY KEY (`id_jornada`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
 ADD PRIMARY KEY (`id_municipio`), ADD KEY `departamento_municipio_fk` (`id_departamento`);

--
-- Indices de la tabla `opciones`
--
ALTER TABLE `opciones`
 ADD PRIMARY KEY (`id_opcion`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
 ADD PRIMARY KEY (`id`), ADD KEY `opciones_permissions_fk` (`id_opcion`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
 ADD PRIMARY KEY (`id_permission_role`), ADD KEY `roles_permission_role_fk` (`role_id`), ADD KEY `permissions_permission_role_fk` (`permission_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
 ADD PRIMARY KEY (`id_role_user`), ADD KEY `roles_role_user_fk` (`role_id`), ADD KEY `users_role_user_fk` (`user_id`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
 ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `alumno_cursos`
--
ALTER TABLE `alumno_cursos`
MODIFY `id_alumno_curso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alumno_cursos_notas`
--
ALTER TABLE `alumno_cursos_notas`
MODIFY `id_alumno_cursos_notas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alumno_pagos`
--
ALTER TABLE `alumno_pagos`
MODIFY `id_alumno_pagos` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
MODIFY `id_bitacora` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `establecimiento`
--
ALTER TABLE `establecimiento`
MODIFY `codigo_establecimiento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
MODIFY `id_jornada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=350;
--
-- AUTO_INCREMENT de la tabla `opciones`
--
ALTER TABLE `opciones`
MODIFY `id_opcion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT de la tabla `permission_role`
--
ALTER TABLE `permission_role`
MODIFY `id_permission_role` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
MODIFY `id_role_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=340;
--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno_cursos`
--
ALTER TABLE `alumno_cursos`
ADD CONSTRAINT `fk_alumno_cursos` FOREIGN KEY (`alumno`) REFERENCES `alumno` (`id`);

--
-- Filtros para la tabla `alumno_pagos`
--
ALTER TABLE `alumno_pagos`
ADD CONSTRAINT `alumno_pagos_ibfk_1` FOREIGN KEY (`alumno`) REFERENCES `alumno` (`id`),
ADD CONSTRAINT `fk_alumno_pagos` FOREIGN KEY (`alumno`) REFERENCES `alumno` (`id`);

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
ADD CONSTRAINT `roles_role_user_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
