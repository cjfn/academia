<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\alumno\alumno;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class alumnos_cursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $query=trim($request->get('searchText'));
        $query2=trim($request->get('searchYear'));
        $cursos=cursos::get();
        $alumnos=alumno::where('nombre','LIKE','%'.$query.'%')->orwhere('apellido','LIKE','%'.$query.'%')
        ->orderBy('fecha_create','DESC')
        ->get();
        /*
        $alumno=alumnos_cursos::rightjoin('cursos as c','c.id_curso','=','alumno_cursos.curso')
        ->select(DB::raw('count(alumno_cursos.alumno) as total'), 'alumno_cursos.id','alumno_cursos.alumno','alumno_cursos.curso','c.id_curso as ccurso','c.nombre')
        ->groupBy('c.id_curso')
        ->get();        
        */
        //select count(*) from alumno_cursos group by curso order by curso asc 
        //select DISTINCT(id_curso) from cursos as c join alumno_cursos as ac on c.id_curso=ac.curso
        $alumno = cursos::join('alumno_cursos','cursos.id_curso','=','alumno_cursos.curso')
        ->groupBy('alumno_cursos.id')
        
        ->select(DB::raw('DISTINCT(cursos.id_curso) as total'), 'cursos.nombre')
           
        ->orderBy('alumno_cursos.curso','ASC')
        ->get();
        return view('alumno_cursos.index',['alumnos'=>$alumno, 'cursos'=>$cursos,'alumno'=>$alumnos,'searchText'=>$query,'searchYear'=>$query2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $cursos=cursos::All();
        return view('alumno_cursos.create',['cursos'=>$cursos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $alumno=alumno_cursos::create($request->all());
        return redirect()->route('alumnos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           if (isset($request['a']))
       {

        
          //select * from cursos as c join alumno_cursos as ac on ac.curso=c.id where ac.alumno=8
        

          $alumnos=cursos::rightJoin('alumno_cursos as ac',' ac.curso','=','cursos.id')
        ->select('cursos.nombre','cursos.id_curso','cursos.hora_inicio', 'cursos.hora_fin','cursos.precio_curso')
        ->where('ac.alumno','=', $request['a'])
        ->get();  
         
        return view('alumno_cursos.show',['alumno'=>$alumnos]);
    }
    }

     public function view(Request $request)
    {
      if($request->ajax()){
                $id = $request->id;
                $info = DB::table('cursos as c')
            ->join('alumno_cursos as ac','ac.curso','=','c.id_curso')
            ->join('alumno as a','a.id','=','ac.alumno')
            ->select('c.nombre','c.descripcion', 'c.fecha_create','ac.hora_inicio','ac.hora_fin','ac.jornada','a.nombre as nombre_alumno','ac.alumno', 'a.apellido')
             ->where('ac.alumno','=',$id)
             
             ->get();//traingo informacion de curso
                //echo json_decode($info);
                return response()->json($info);
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

}
