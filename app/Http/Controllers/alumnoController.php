<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumno\alumno;
use App\model\departamento\departamento;
use App\model\municipio\municipio;
use App\model\establecimiento\establecimiento;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use App\model\cursos\cursos;
use App\model\cursos_tipos_pagos\cursos_tipos_pagos;
use Carbon\Carbon;

class alumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
       
        
        //$alumno=alumno::all();

        /*
        select * from alumno as a join departamento as d on a.departamento=d.id_departamento
        join municipio as m on a.municipio=m.id_municipio
        join establecimiento as e on a.establecimiento=e.codigo_establecimiento
        */

        $query2=trim($request->get('searchYear'));
        if($query2==null)
        {
          $fecha_actual = Carbon::now();
          $month = $fecha_actual->format('m');
          $day = $fecha_actual->format('d');
          $year = $fecha_actual->format('Y');
          $date = $fecha_actual->format('Y-m-d');

          $query2=$year;
           $alumnos=DB::table('alumno as a')->join('departamento as d', 'a.departamento', '=', 'd.id_departamento')          
        ->join('municipio as m', 'a.municipio', '=', 'm.id_municipio')          
        ->join('establecimiento as e', 'a.establecimiento', '=', 'e.id')          
          ->select('a.id','a.nombre','a.apellido','a.telefono','a.direccion','a.fecha_nacimiento','a.ciclo','a.activo','a.fecha_create','d.nombre_departamento as detpo','m.nombre_municipio as mun','e.nombre as estab')
          ->where('a.ciclo','LIKE',$query2)
          
          ->orderBy('a.id','desc')
          // ->Orwhere('a.ciclo','LIKE', '%'.$query2.'%')
          //->paginate(10);
          ->get();
        }
        else
        {
           $alumnos=DB::table('alumno as a')->join('departamento as d', 'a.departamento', '=', 'd.id_departamento')          
        ->join('municipio as m', 'a.municipio', '=', 'm.id_municipio')          
        ->join('establecimiento as e', 'a.establecimiento', '=', 'e.id')          
          ->select('a.id','a.nombre','a.apellido','a.telefono','a.direccion','a.fecha_nacimiento','a.ciclo','a.activo','a.fecha_create','d.nombre_departamento as detpo','m.nombre_municipio as mun','e.nombre as estab')
          ->where('a.ciclo','LIKE',$query2)
          
          ->orderBy('a.id','desc')
          // ->Orwhere('a.ciclo','LIKE', '%'.$query2.'%')
          //->paginate(10);
          ->get();
        }
       
        return view('alumno.index',['alumnos'=>$alumnos,  'searchYear'=>$query2]);
    }

    public function search(Request $request)
    {
        if($request->ajax())
        {
            $alumnos=DB::table('alumno as a')->join('departamento as d', 'a.departamento', '=', 'd.id_departamento')          
        ->join('municipio as m', 'a.municipio', '=', 'm.id_municipio')          
        ->join('establecimiento as e', 'a.establecimiento', '=', 'e.id')          
          ->select('a.id','a.nombre','a.apellido','a.telefono','a.direccion','a.fecha_nacimiento','a.ciclo','a.fecha_create','d.nombre_departamento as detpo','m.nombre_municipio as mun','e.nombre as estab')
          ->where('a.ciclo','=',$request->get('searchYear'))
          ->where('a.activo','=', 1)
          ->get();

          if($alumnos)
          {
            foreach ($alumnos as $key => $alumno) {
                $output.='<tr>'.
                            '<td>'.$alumno->id.'</td>'.
                            '<td>'.$alumno->nombre.'</td>'.
                            '<td>'.$alumno->apellido.'</td>'.
                            '<td>'.$alumno->telefono.'</td>'.
                            '</tr>';
                           
            }
            return Response($output);
          }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departamento=departamento::All();
        $municipio=municipio::All();
        $establecimiento=establecimiento::All();
        $d = array();
        $m = array();
        $e = array();
        foreach ($departamento as $key => $row) {
            $d[$row->id_departamento] = mb_strtoupper($row->nombre_departamento);
          }

        foreach ($municipio as $key => $row) {
            $m[$row->id_municipio] = mb_strtoupper($row->nombre_municipio);
          }

        foreach ($establecimiento as $key => $row) {
            $e[$row->id] = mb_strtoupper($row->nombre);
          }

        Session:flash('save','Se ha creado correctamente');
        return view('alumno.create',['departamentos'=>$d,'municipio'=>$m,'establecimiento'=>$e]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno=alumno::create($request->all());
        
       $cursos=cursos::All();
       $c=array();
        foreach ($cursos as $key => $row) {
            $c[$row->id] = mb_strtoupper($row->nombre);
          }

        $alumno=alumno::orderBy('id', 'desc')->select('id')->first();
        $id=$alumno['id'];
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $grado = $request->grado;


    return redirect()->action('alumnos_cursosController@index');
        //return redirect()->route('alumnos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        

         $alumnos=DB::table('alumno as a')->join('departamento as d', 'a.departamento', '=', 'd.id_departamento')
        ->join('municipio as m', 'a.municipio', '=', 'm.id_municipio')          
        ->join('establecimiento as e', 'a.establecimiento', '=', 'e.id')          
          ->select('a.id','a.nombre','a.apellido','a.telefono','a.direccion','a.fecha_nacimiento','a.ciclo','a.fecha_create','d.nombre_departamento as depto','m.nombre_municipio as mun','e.nombre as esta')
          ->where('a.id','=',$id)
           ->first();

            $cursos=DB::table('cursos as c')->join('alumno_cursos as ac', 'c.id_curso', '=', 'ac.curso')
        ->select('c.nombre','ac.hora_inicio','ac.hora_fin','ac.alumno')
        ->where('ac.alumno','=',$id)
        ->get();
           
         return view('alumno.show',['alumno'=>$alumnos,'cursos'=>$cursos]);     

          
          // ->Orwhere('a.ciclo','LIKE', '%'.$query2.'%')
         
       
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamento=departamento::All();
        $municipio=municipio::All();
        $establecimiento=establecimiento::All();
        $d = array();
        $m = array();
        $e = array();
        foreach ($departamento as $key => $row) {
            $d[$row->id_departamento] = mb_strtoupper($row->nombre_departamento);
          }

        foreach ($municipio as $key => $row) {
            $m[$row->id_municipio] = mb_strtoupper($row->nombre_municipio);
          }

        foreach ($establecimiento as $key => $row) {
            $e[$row->id] = mb_strtoupper($row->nombre);
          }

        $alumno=alumno::where('id', $id)->first();// busco alumno por id
        return view('alumno.edit',['alumno'=>$alumno,'departamentos'=>$d,'municipio'=>$m,'establecimiento'=>$e]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$alumno=alumno::where('id', $id)->first();// busco alumno por id
        
        $alumno=alumno::findOrFail($id);
        $input = $request->all();
        $alumno->fill($input)->save();
        
        /*
        $clave=$request['clave'];
        $codigo=$request['codigo'];
        $nombre=$request['nombre'];
        $apellido=$request['apellido'];
        $departamento=$request['departamento'];
        $municipio=$request['municipio'];
        $establecimiento=$request['establecimiento'];
        $telefono=$request['telefono'];
        $direccion=$request['direccion'];
        $fecha_nacimiento=$request['fecha_nacimiento'];

           DB::table('alumno')->where('id', $id)
            ->update([
                   
                     'clave'=>$clave,
                    'codigo'=>$codigo,
                    'nombre'=>$nombre,
                    'apellido'=>$apellido,
                    'departamento'=>$departamento,
                    'municipio'=>$municipio,
                    'establecimiento'=>$establecimiento,
                    'telefono'=>$telefono,
                    'direccion'=>$direccion,
                    'fecha_nacimiento'=>$fecha_nacimiento
            ]);
            */
        Session:flash('update','Se ha actualizado correctamente');
        return redirect()->route('alumnos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
            $alumno=alumno::findOrFail($id);
            $alumno->delete();
            Session:flash('delete','Se ha eliminado correctamente');
            return redirect()->route('alumnos.index');       

    }

      /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = alumno::find($id);
            $data->activo = 0;
            $respones = $data ->save();
            //$response = $data -> delete();
            if($response)
                echo "Record Deleted successfully.";

            else
                echo "There was a problem. Please try again later.";
        }

          /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = alumno::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }


    public function bscautomplete(Request $request)
    {
    $q = $request['query'];
     $d = array();
    $datos = alumno::where('nombre','LIKE','%'.$term.'%')
        ->take(10)
        ->get();
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombre.' '.$row->apellido),'data'=>$row->id);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }


   //busqueda select 2 no tocar

    public function dataAjax(Request $request)
    {
      $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("alumno")
                ->select("id","nombre","apellido")
                ->where('nombre','LIKE',"%$search%")
                ->get();
        }

        return response()->json($data);
    }

    //prueba cargar nombres con ajax

    public function findAlumnosNombres(Request $request){
     
            $data = DB::table("alumno")
                ->select("id","nombre","apellido")
                ->where('id','=',$request->id)
                ->get();
                
        

        return response()->json($data);
    }

    //select con todos los cursos que tiene asignado el alumno

    /* select * from alumno_cursos as acc join cursos as c on acc.curso=c.id 
where acc.alumno=1
  */

      public function findAlumnosCursos(Request $request)
        {
             $data = DB::table("alumno_cursos as acc")
                ->join("cursos as c","acc.curso","=","c.id_curso")
                ->select("acc.hora_inicio")
                ->where('acc.alumno','=',$request->id)
                ->get();
                
        

        return response()->json($data);
           
        }




}
