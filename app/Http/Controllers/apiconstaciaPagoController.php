<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\model\pagos\pagos;
use DB;

class apiconstaciaPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
          /*
        select * from alumno_pagos
RIGHT join alumno on alumno_pagos.alumno=alumno.id
left join cursos on alumno_pagos.curso=cursos.id_curso
where id_alumno_pagos=11
*/
        
        $data = pagos::rightJoin('alumno','alumno_pagos.alumno','=','alumno.id')
        ->leftjoin('cursos','alumno_pagos.curso','=','cursos.id_curso')
        ->where('alumno_pagos.activo','=',1)
        ->where('id_alumno_pagos','=', $id)
        ->select('alumno_pagos.*')
        ->take(1)
        ->get();


            return view('pdf.constanciapago',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
