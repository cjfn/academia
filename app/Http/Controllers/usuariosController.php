<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UsuarioRequest;
use App\model\usuarios\usuarios;
use App\roles;
use DB;
use Hash;
use Auth;


class usuariosController extends Controller
{
      public function index()
    {
         $data = usuarios::join('roles as r','users.tipo_usuario','=','r.id')->get();
         $roles = roles::all();
            return view('usuario.index',['data'=>$data,'roles'=>$roles]);
    }

   public function add(Request $request)
        {
            $data = new usuarios;
            $data -> name = $request -> name;
            $data -> email = $request -> email;
            $data -> password = bcrypt($request -> password);
            $data -> tipo_usuario = $request -> tipo_usuario;
            $data -> save();
            return back()
                    ->with('success','Record Added successfully.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = usuarios::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {

            $id = $request -> edit_id;
            $data = usuarios::find($id);
            $data -> name = $request -> edit_name;
            $data -> email = $request -> edit_email;
            $data -> password = bcrypt($request -> edit_password);
            $data -> tipo_usuario = $request -> edit_tipo_usuario;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = usuarios::find($id);
            $response = $data -> delete();
            if($response)
                echo "Record Deleted successfully.";
            else
                echo "There was a problem. Please try again later.";
        }

    public function create()
    {
    	
        
          $roles = roles::All();

          foreach ($roles as $key => $row) 
          {

            $r[$row->id] = mb_strtoupper($row->name);

          }

          return view('usuario.nusuario', ['roles'=>$r]);

       

    }

    

 public function store(UsuarioRequest $request)


    {

           $user = User::create($request->all());
           \Flash::success("Se ha creado el usuario $user->name de forma correcta");
           return redirect()->route('usuario'); 
            
            
      }
        

        
/*




    public function store(UsuarioRequest $request)
    {
    	//crear el arreglo de los mensajes de validacion


     
      try {

       
    DB::table('users')->insert([

                                  'name'=>$request['name'],

                                  'email'=>$request['email'],

                                  'password'=>$request['password'],

                                  'tipo_usuario'=>$request['tipo_usuario']

                                 

                                  

                                ]);


      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'Usuario registrado exitosamente!!!');

        return redirect('/usuarios');

      
    }

*/

    /*
     public function store(Request $request)
      {
        $Usuario = new User;
        $Usuario->name=$request->input('name');
        $Usuario->password=$request->input('password');
        $Usuario->email=$request->input('email');
        $Usuario->tipo_usuario=$request->input('tipo_usuario');
        $Usuario->save();
        return 'Usuario creado exitosamente con el id'. $Usuario->id;
      }
    */
}