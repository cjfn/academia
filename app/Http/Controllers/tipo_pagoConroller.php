<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\tipo_pago\tipo_pago;

class tipo_pagoConroller extends Controller
{
    //
    public function index()
    {
    		$data= tipo_pago::all();

            return view('tipo_pago.index',['data'=>$data]);
    }

     public function add(Request $request)
        {
            $data = new tipo_pago;
            $data -> nombre = $request -> nombre;
            $data -> tipo_pago = $request -> tipo_pago;
            $data -> pago = $request -> pago;
            $data -> observaciones = $request -> observaciones;
            $data -> anio = $request -> anio;
            $data -> save();
            return back()
                    ->with('success','Record Added successfully.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = tipo_pago::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = tipo_pago::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> tipo_pago = $request -> edit_tipo_pago;
            $data -> pago = $request -> edit_pago;
            $data -> observaciones = $request -> edit_observaciones;
            $data -> anio = $request -> edit_anio;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');
        }

         public function delete(Request $request)
        {
            $id = $request -> id;
            $data = tipo_pago::find($id);
            $data->activo = 0;
            $respones = $data ->save();
            //$response = $data -> delete();
            if($response)
                echo "Tipo de pago inactivado exitosamente.";

            else
                echo "There was a problem. Please try again later.";
        }


 
}
