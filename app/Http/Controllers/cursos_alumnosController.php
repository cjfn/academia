<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\alumno\alumno;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class cursos_alumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (isset($request['a']))
       {

        
          
        //select * from alumno as a join alumno_cursos as ac on a.id=ac.alumno where ac.curso=3
         
        $curso=$request['curso'];

         $alumno=DB::table('alumno as a')
            ->join('alumno_cursos as ac','a.id','=','ac.alumno')
             ->where('ac.curso','=',$request['a'])
             ->where('ac.jornada','=',1)
             ->orderBy('hora_inicio', 'asc')

             
             ->get();//traingo informacion de unidades

             //alumno jornada fin de semana

              $alumno_finde=DB::table('alumno as a')
            ->join('alumno_cursos as ac','a.id','=','ac.alumno')
             ->where('ac.curso','=',$request['a'])
             ->where('ac.jornada','=',2)
             ->orderBy('hora_inicio', 'asc')

             
             ->get();//traingo informacion de unidades
         $alumnos=alumno::all();
         $a=$request['a'];
       
         

        return view('cursos_alumno.index',['alumno'=>$alumno, 'cursos_alumno'=>$alumnos, 'a'=>$a, 'curso'=>$curso, 'alumno_finde'=>$alumno_finde]);
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
