<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\alumno\alumno;
use App\model\cursos\cursos;
use DB;
use Session;
use PDF;
class reportsController extends Controller
{
    //
    public function index()
    {
    	return('reports.invoice');
    }

    //FUNCION DE REPORTES PDF
  public function invoice() 

    {

        $data = $this->crear_reporte_cursos();
        $date = date('d-m-Y');
        $view =  \View::make('reports.invoice', compact('data',  'date'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');

    }


    //ASIGNACION DE CURSOS
    public function getData() 

    {
    	//select * from alumno_cursos as ac join alumno as a on a.id=ac.alumno
    	
    	$cursoalumnosdata=DB::table('alumno_cursos as ac')
                ->join('alumno as a','a.id','=','ac.alumno')
                 ->select('a.id as id', 'a.nombre','a.apellido','ac.hora_inicio', 'ac.hora_fin')                 
                 ->get();
                 /*
                 $cursoalumnosdata=DB::table('alumno_cursos as ac')->get();
                 */

    	/*
        $cursoalumnosdata = alumno::join('alumno_cursos as ac','alumno.id','=','ac.alumno')
             ->orderBy('hora_inicio', 'ASC')
             ->select('alumno.id as id', 'alumno.nombre','alumno.apellido','ac.hora_inicio', 'ac.hora_fin') 
             ->get();//traingo informacion de unidades//datos de estudiante
             */
  $data =[
          'id'=>$cursoalumnosdata->id,
          'nombre' => $cursoalumnosdata->nombre,
          'apellido' => $cursoalumnosdata->apellido,
          'hora_inicio' => $cursoalumnosdata->hora_inicio,
          'hora_fin' => $cursoalumnosdata->hora_fin

         
        ];
        return $data;
        }

        public function crear_reporte_cursos()
        {
        	
        	$data=alumno::all();

        	return $data;
        }
}
