<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumno\alumno;
use App\model\salon\Salon;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class SalonController extends Controller
{
     public function index()
    {
         $data = Salon::all();
            return view('salon.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new Salon;
            $data -> nombre = $request -> nombre;
            $data -> descripcion = $request -> descripcion;
            $data -> limite_alumnos = $request -> limite_alumnos;
            $data -> save();
            return back()
                    ->with('success','Record Added successfully.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = Salon::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = Salon::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> descripcion = $request -> edit_descripcion;
            $data -> limite_alumnos = $request -> edit_limite_alumnos;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = Salon::find($id);
            $response = $data -> delete();
            if($response)
                echo "Record Deleted successfully.";
            else
                echo "There was a problem. Please try again later.";
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $create = cursos::create($request->all());
        return response()->json($create);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Salon::find($id)->delete();
        return response()->json(['done']);
    }
}
