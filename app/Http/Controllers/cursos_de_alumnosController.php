<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\alumno\alumno;
use App\model\salon\Salon;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class cursos_de_alumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (isset($request['a']))
       {

        
        
          
        //select * from cursos as c join alumno_cursos as ac on ac.curso=c.id where ac.alumno=8
         


         $cursos_alumnos=DB::table('cursos as c')
            ->join('alumno_cursos as ac','ac.curso','=','c.id_curso')
            ->select('c.nombre','c.descripcion', 'c.fecha_create','ac.hora_inicio','ac.hora_fin','ac.jornada')
             ->where('ac.alumno','=',$request['a'])
             
             ->get();//traingo informacion de cursos
        
         

        return view('cursos_de_alumnos.index',['alumno'=>$cursos_alumnos]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
          if (isset($request['a']))
       {

        
       $cursos=cursos::get();
       $c=array();
        foreach ($cursos as $key => $row) {
            $c[$row->id] = mb_strtoupper($row->nombre);
          }

        $salon = 1;

        
         

        return view('cursos_de_alumnos.create',['cursos'=>$cursos,'alumno'=>$request['a'],'nombre'=>$request['nombre'],'apellido'=>$request['apellido'],'salon'=>$salon,'grado'=>$request['grado']]);
        }
    }

     public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = DB::table('cursos as c')
            ->join('alumno_cursos as ac','ac.curso','=','c.id_curso')
            ->select('c.nombre','c.descripcion', 'c.fecha_create','ac.hora_inicio','ac.hora_fin','ac.jornada')
             ->where('ac.alumno','=',$id)
             
             ->get();//traingo informacion de curso
                //echo json_decode($info);
                return response()->json($info);
            }
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = new alumnos_cursos;
        $data->alumno = $request->alumno;
        $data->curso = $request->curso;
        $data->hora_inicio = $request->hora_inicio;
        $data->hora_fin = $request->hora_fin;
        $data->jornada = $request->jornada;
        

        $limite=Salon::where('id','=',1)->select('limite_alumnos')->firstOrFail();//30;



        $total = alumnos_cursos::select('alumno')->where('hora_inicio','=',$request->hora_inicio)
        ->where('jornada','=',$request->jornada)->get();//46;

        $total_inscritos = DB::table('alumno_cursos')
        ->select(DB::raw('COUNT(*) as totalinscritos'))
        ->where('hora_inicio','=',$request->hora_inicio)
        ->where('jornada','=',$request->jornada)
        //->where('salon','=',$request->salon)
        ->first();
       

        if($limite['limite_alumnos']>=count($total)/*$total_inscritos->totalinscritos*/)
        {

            $query2=trim($request->get('searchYear'));
            //$alumno_cursos=alumnos_cursos::create($request->all());
            $data->save();
            Session::flash('message', 'CURSO AGREGADO EXITOSAMENTE'); 
            Session::flash('alert-class', 'alert-success');
            return redirect()->action('alumnos_cursosController@index');

             
        }
        else
        {

                Session::flash('message', 'CURSO NO PUEDE SER AGREGADO LIMITE DE ALUMNOS EXCEDIDO PARA ESTE HORARIO'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect()->action('alumnos_cursosController@index');

        }


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
