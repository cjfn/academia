<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\alumnos_cursos\alumnos_cursos;
use DB;

class alumnoCursoController extends Controller
{
    //
     public function add(Request $request)
    {
    	  $data = new alumnos_cursos;
        $data->alumno = $request->id;
        $data->curso = $request->cursos;
        $data->hora_inicio = $request->hora_inicio;
        $data->hora_fin = $request->hora_fin;
        $data->jornada = $request->jornada;
        $data->save();
          return back()
                    ->with('success','Producto ingresado exitosamente con codigo de barras No.'.$data->id);
	
    }
}
