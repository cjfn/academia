<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\cursos_tipos_pagos\cursos_tipos_pagos;
use App\model\tipo_pagos\tipo_pagos;
use App\model\cursos\cursos;
use DB;
use Session;

class cursos_tipos_pagosController extends Controller
{
    public function index()
    {
    	//$query=trim($request->get('searchText'));
        $data = cursos_tipos_pagos::join('cursos as c', 'cursos_tipos_pagos.cursos', '=', 'c.id')
        ->join('tipo_pagos','cursos_tipos_pagos.tipo_pagos','=','tipo_pagos.id')        
        ->select('cursos_tipos_pagos.id','cursos_tipos_pagos.cursos','cursos_tipos_pagos.tipo_pagos','cursos_tipos_pagos.anio','cursos_tipos_pagos.pago','cursos_tipos_pagos.observaciones','cursos_tipos_pagos.fecha_created', 'c.nombre as curso_nombre', 'tipo_pagos.nombre as tipo_pagos_nombre','tipo_pagos.observaciones') 
        ->groupBy('cursos_tipos_pagos.id') 
        ->orderBy('cursos_tipos_pagos.cursos', 'DESC')  
        ->where('cursos_tipos_pagos.activo','=','1')
        ->get();
         $cursos = cursos::all();
         $tipo_pagos=tipo_pagos::where('activo','=','1')->get();
         

            return view('cursos_tipos_pagos.index',['data'=>$data,'cursos'=>$cursos,'tipo_pagos'=>$tipo_pagos]);

    }

   public function add(Request $request)
        {
            $data = new cursos_tipos_pagos;
            $data -> cursos = $request -> curso;
            $data -> tipo_pagos = $request -> tipo_pagos;
            $data -> anio = $request -> anio;
            $data -> pago = $request -> pago;
            $data -> observaciones = $request -> observaciones;
            $data -> save();
            return back()->with('Finalizado','Tipo de pago para curso asignado exitosamente!');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                //$info = cursos_tipos_pagos::find($id);
                $info = cursos_tipos_pagos::join('cursos as c', 'cursos_tipos_pagos.cursos', '=', 'c.id')
                ->join('tipo_pagos','cursos_tipos_pagos.tipo_pagos','=','tipo_pagos.id')        
                ->select('cursos_tipos_pagos.id','cursos_tipos_pagos.cursos','cursos_tipos_pagos.tipo_pagos','cursos_tipos_pagos.anio','cursos_tipos_pagos.pago','cursos_tipos_pagos.observaciones','cursos_tipos_pagos.fecha_created', 'c.nombre as curso_nombre', 'tipo_pagos.nombre as tipo_pagos_nombre','tipo_pagos.observaciones') 
                ->find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = cursos_tipos_pagos::find($id);
            $data -> cursos = $request -> cursos;
            $data -> tipo_pagos = $request -> tipo_pagos;
            $data -> anio = $request -> anio;
            $data -> pago = $request -> pago;
            $data -> observaciones = $request -> observaciones;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = cursos_tipos_pagos::find($id);
            $response = $data -> delete();
            if($response)
                echo "Record Deleted successfully.";
            else
                echo "There was a problem. Please try again later.";
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $create = cursos_tipos_pagos::create($request->all());
        return response()->json($create);
    }

    public function autocomplete(){
	$term = Input::get('term');
	
	$results = array();
	
	$queries = DB::table('cursos')
		->where('first_name', 'LIKE', '%'.$term.'%')
		->orWhere('last_name', 'LIKE', '%'.$term.'%')
		->take(5)->get();
	
	foreach ($queries as $query)
	{
	    $results[] = [ 'id' => $query->id, 'value' => $query->first_name.' '.$query->last_name ];
	}
return Response::json($results);
}

}
