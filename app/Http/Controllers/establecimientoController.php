<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\establecimiento\establecimiento;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class establecimientoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = establecimiento::all();
            return view('establecimiento.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new establecimiento;
            $data -> nombre = $request -> nombre;
            $data -> direccion = $request -> direccion;
            $data -> save();
            return back()
                    ->with('Finalizado','Establecimiento agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = establecimiento::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = establecimiento::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> direccion = $request -> edit_direccion;
            $data -> save();
            return back()
                    ->with('Finalizado','Establecimiento modificado exitosamente!');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = establecimiento::find($id);
            $response = $data -> delete();
            if($response)
                echo "Establecimiento eliminado exitosamente.";
            else
                echo "Hubo un problema, intenta mas tarde!.";
        }

}
