<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumno\alumno;
use App\model\pagos\pagos;
use App\model\tipo_pago\tipo_pago;
use App\model\cursos_tipos_pagos\cursos_tipos_pagos;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;


class pagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *      
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query=trim($request->get('searchText'));
        $data = pagos::join('alumno as a', 'alumno_pagos.alumno', '=', 'a.id')
        ->join('tipo_pagos as tp','alumno_pagos.tipo_pagos','=','tp.id')
        ->join('cursos as c','tp.curso','=','c.id_curso')        
        ->select('alumno_pagos.id_alumno_pagos as id','alumno_pagos.alumno as ida','a.id as idalumno','a.nombre','a.apellido','alumno_pagos.id_alumno_pagos','alumno_pagos.total', 'tp.nombre as nombre_tipo_pagos','alumno_pagos.forma_pago', 'alumno_pagos.fecha_create as fecha_pago', 'alumno_pagos.activo', 'c.nombre as nombre_curso') 
        ->orderBy('alumno_pagos.fecha_create', 'DESC') 
        ->where('a.nombre','LIKE', '%'.$query.'%')
        ->Orwhere('a.apellido','LIKE', '%'.$query.'%')
        ->Orwhere('a.ciclo','LIKE', '%'.$query.'%')  
        ->orderBy('alumno_pagos.fecha_create','desc') 
        ->paginate(10);
         $alumno = alumno::all();
         $tipo_pagos=tipo_pago::where('activo','=','1')->get();
         $cursos_tipos_pagos=cursos_tipos_pagos::join('tipo_pagos as tp','cursos_tipos_pagos.tipo_pagos','=','tp.id')
         ->join('cursos as c','cursos_tipos_pagos.cursos','=','c.id_curso')
         ->select('cursos_tipos_pagos.id','cursos_tipos_pagos.anio','cursos_tipos_pagos.pago','c.nombre as nombre_pago', 'tp.nombre as tipo_pago')
         ->where('cursos_tipos_pagos.activo','=','1')->get();

         

            return view('pagos.index',['data'=>$data,'alumno'=>$alumno,'tipo_pagos'=>$tipo_pagos,'searchText'=>$query,'cursos_tipos_pagos'=>$cursos_tipos_pagos]);
    }

     public function view(Request $request)
        {
             if($request->ajax()){
                $id = $request->id;
                $info = pagos::join('tipo_pagos','alumno_pagos.tipo_pagos','=','tipo_pagos.id')
                ->join('cursos','tipo_pagos.curso','=','cursos.id_curso')
                ->leftjoin('alumno','alumno_pagos.alumno','=','alumno.id')
                ->select('alumno.nombre', 'alumno.apellido','tipo_pagos.nombre as nombre_tipo_pagos','tipo_pagos.pago','alumno_pagos.fecha_create')
                ->where('id_alumno_pagos','=',$id)
                ->get();
                //echo json_decode($info);
                return response()->json($info);
            }
           
        }

     public function add(Request $request)
        {
            $data = new pagos;
            $data -> alumno = $request -> alumno;
            $data -> tipo_pagos = $request -> tipo_pagos;
            $tipo_pago = tipo_pago::where('id','=', $request -> tipo_pagos)->first();
            $total = $tipo_pago -> pago;
            $data -> total = $total;
            $data -> forma_pago = $request -> forma_pago;
            $data -> año = $request -> año;
            $ndata = tipo_pago::orderBy('id_alumno_pagos','DESC')->first();
            
            //$data -> nombre = $request -> nombre;
            //$data -> direccion = $request -> direccion;
            $data -> save();
            $id = $ndata->id_alumno_pagos;


            /*
            return back()
                    ->with('success','Pago acreditado exitosamente  por Q.'. $data -> total . ' exactos');

            */


            return redirect()->action('apiconstaciaPagoController@show', [$id]);

        }

        /*
         * View data
         */
     



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $tipo_pagos= tipo_pagos::all();
         $tp = array();
          foreach ($tipo_pagos as $key => $row) {
            $tp[$row->id] = mb_strtoupper($row->nombre);
          }

          return view('pagos.create',['tipo_pagos'=>$tp,'alumno'=>$request['a']]);
          

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno_cursos=pago::create($request->all());
        return redirect()->route('pagos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          
        $tipo_pagos= tipo_pagos::all();
        $tp = array();
        foreach ($tipo_pagos as $key => $row) {
            $tp[$row->id] = mb_strtoupper($row->nombre);
          }

          $alumno=alumno::where('id', $id)->first();// busco alumno por id
          return view('pagos.edit',['tipo_pagos'=>$tp,'alumno'=>$alumno]);
          
    }

  



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
     
     

        
    }

   
    public function destroy($id)
    {
        //
    }

    public function detalles(Request $request)
    {
        $tipo_pagos=tipo_pagos::all();
         $detalles_pago = alumno_pagos::where('id_alumno_pagos','=',$request['a']);
         

            return view('pagos.detalles',['data'=>$detalles_pago,'tipo_pagos'=>$tipo_pagos]);

    }

     public function constanciapago($id)
    {
         //traigo todos los alumnos con cursos  para pdf

        /*
        select * from alumno_pagos
RIGHT join alumno on alumno_pagos.alumno=alumno.id
left join cursos on alumno_pagos.curso=cursos.id_curso
where id_alumno_pagos=11
*/
        
        $data = pagos::rightJoin('alumno','alumno_pagos.alumno','=','alumno.id')
        ->leftjoin('cursos','alumno_pagos.curso','=','cursos.id_curso')
        ->where('alumno_pagos.activo','=',1)
        ->where('id_alumno_pagos','=', $id)
        ->select('alumno_pagos.*')
        ->take(1)
        ->get();


            return view('pdf.constanciapago',['data'=>$data]);
    }

     public function getData() 
    {
        /*
        $cursoalumnosdata=DB::table('alumno as a')
                ->select('a.nombre','a.apellido')
                ->orderBy('a.id', 'desc')
                 
                 ->get();
                 */

                 $cursoalumnosdata=alumno::all();

        $data =  [
          'nombre' => $cursoalumnosdata->nombre,
          'apellido' => $cursoalumnosdata->apellido
          //'hora_inicio' => $cursoalumnosdata['hora_inicio'],
          //'hora_fin' => $cursoalumnosdata['hora_fin']

        ];
        return $data;
    }

    //on select change cursos de alumnos
     /**
     * Show the application selectAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectAjax(Request $request)
    {

        //seleccionar todos los tipos de pagos que esten en cursos_tipos_pagos asignados a alumnos_cursos donde este el alumnos seleccionado
        /*
        select DISTINCT(tp.id), tp.nombre, tp.tipo_de_pago from tipo_pagos as tp join cursos_tipos_pagos as ctp on tp.id=ctp.tipo_pagos
        join alumno_cursos as ac on ctp.cursos=ac.curso
        where  ac.alumno=1
        order by tp.id

        */
        if($request->ajax()){
            $tipo_pagos = DB::table('tipo_pagos as tp')->join('cursos_tipos_pagos as ctp','tp.id','=','ctp.tipo_pagos')
            ->join('alumno_cursos as ac','ctp.cursos','=','ac.curso')->where('ac.alumno',$request->id_country)->pluck("tp.nombre","tp.id")->all();
            $data = view('pagos.ajax-select',compact('tipo_pagos'))->render();
            return response()->json(['options'=>$data]);
        }
    }

}
