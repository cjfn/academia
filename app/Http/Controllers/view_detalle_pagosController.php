<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumno\alumno;
use App\model\pagos\pagos;
use App\model\cursos_tipos_pagos\cursos_tipos_pagos;

class view_detalle_pagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $alumno= $request['a'];
        $nombre= $request['b'];
        $apellido= $request['c'];


        //todos los cursos pagados de los alumnos por id, nombre, apellido, ciclo
        /*
        SELECT * FROM `alumno_pagos` as ap join alumno a on ap.alumno=a.id
        join cursos_tipos_pagos as ctp on ap.cursos_tipos_pagos=ctp.id
        join cursos as c on ctp.cursos=c.id
        join tipo_pagos tp on ctp.tipo_pagos=tp.id
        WHERE
        a.id=2 and a.apellido='Gomez Bolaños' and a.nombre='Juan Pablo' and a.ciclo=2017
        */

        $cursos_pagados= pagos::join ('alumno as a','alumno_pagos.alumno','=','a.id')
        ->join('cursos_tipos_pagos as ctp','alumno_pagos.cursos_tipos_pagos','=','ctp.id')
        ->join('cursos as c','ctp.cursos','=','c.id')
        ->join('tipo_pagos as tp','ctp.tipo_pagos','=','tp.id')
        ->where('a.id','=', $alumno)
        ->where('a.apellido','=',$nombre)
        ->where('a.nombre','=',$apellido)
        ->select('alumno_pagos.id_alumno_pagos as id','c.nombre','alumno_pagos.total','a.grado','a.ciclo','alumno_pagos.fecha_create','tp.tipo_de_pago')
        ->get();

        return view('pagos.detalles', ['detalles_pago'=>$cursos_pagados,'nombres'=>$nombre,'apellidos'=>$apellido]);

    }

      public function view(Request $request)
        {
            if($request->ajax())
            {
                $id = $request->id;
                $info = pagos::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
