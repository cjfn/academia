<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\tipo_pago\tipo_pago;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class tipo_pagoController extends Controller
{
    //
    public function index()
    {
    		//$data= tipo_pago::where('activo','=',1)->get();
            $data= tipo_pago::join('cursos','cursos.id_curso','=','tipo_pagos.curso')
            ->select('tipo_pagos.*','cursos.nombre as nombre_curso')
            ->get();
            $cursos = cursos::where('activo','=',1)->get();
            return view('tipo_pago.index',['data'=>$data, 'cursos'=>$cursos]);
    }
     public function add(Request $request)
        {
            $data = new tipo_pago;
            $data -> nombre = $request -> nombre;
            $data -> curso = $request -> curso;
            $data -> pago = $request -> pago;
            $data -> tipo_pago = $request -> tipo_pago;
            $data -> observaciones = $request -> observaciones;
            $data -> save();
            return back()
                    ->with('success','Record Added successfully.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = tipo_pago::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = tipo_pago::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> pago = $request -> edit_pago;
            $data -> tipo_pago = $request -> edit_tipo_de_pago;
            $data -> observaciones = $request -> edit_observaciones;
            return back()
                    ->with('success','Record Updated successfully.');
        }

         public function delete(Request $request)
        {
            $id = $request -> id;
            $data = tipo_pago::find($id);
            $data->activo = 0;
            $respones = $data ->save();
            //$response = $data -> delete();
            if($response)
                echo "Tipo de pago inactivado exitosamente.";

            else
                echo "There was a problem. Please try again later.";
        }

}
