<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\model\alumnos_cursos\alumnos_cursos;
use App\model\alumno\alumno;
use App\model\cursos\cursos;
use DB;
use Illuminate\Support\Facades\Input;
use Session;


class PdfController extends Controller
{

   



    public function invoice() 
    {

        //traigo todos los alumnos con cursos  para pdf
        $data = alumno::join('departamento as d','alumno.departamento','=','d.id_departamento')
        ->join('municipio as m', 'alumno.municipio','=','m.id_municipio')
        ->join('alumno_cursos as ac','alumno.id','=','ac.alumno')
        ->join('cursos as c','ac.curso','=','c.id')
        ->join('jornada as j','ac.jornada','=','j.id_jornada')
        ->where('alumno.activo','=',1)
        ->select('alumno.id as codigo', 'alumno.nombre as nombre','alumno.apellido', 'alumno.telefono','alumno.direccion','alumno.grado','alumno.activo','d.nombre_departamento','c.nombre as curso','ac.hora_inicio', 'ac.hora_fin','j.nombre as jornada')
        ->orderBy('j.id_jornada', 'asc')
        ->orderBy('ac.hora_inicio', 'asc')
        ->get();

        //total de alunos
        $data_total = alumno::join('departamento as d','alumno.departamento','=','d.id_departamento')
        ->join('alumno_cursos as ac','alumno.id','=','ac.alumno')
        ->join('cursos as c','ac.curso','=','c.id')
        ->join('jornada as j','ac.jornada','=','j.id_jornada')
        ->where('alumno.activo','=',1)
        ->select('alumno.id as codigo', 'alumno.nombre as nombre','alumno.apellido', 'alumno.telefono','alumno.direccion','alumno.grado','alumno.activo','d.nombre_departamento','c.nombre as curso','ac.hora_inicio', 'ac.hora_fin','j.nombre as jornada')
        ->count();
        $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoice', compact('data', 'data_total', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');
    }
 
    public function getData() 
    {
        /*
        $cursoalumnosdata=DB::table('alumno as a')
                ->select('a.nombre','a.apellido')
                ->orderBy('a.id', 'desc')
                 
                 ->get();
                 */

                 $cursoalumnosdata=alumno::all();

        $data =  [
          'nombre' => $cursoalumnosdata->nombre,
          'apellido' => $cursoalumnosdata->apellido
          //'hora_inicio' => $cursoalumnosdata['hora_inicio'],
          //'hora_fin' => $cursoalumnosdata['hora_fin']

        ];
        return $data;
    }
}
