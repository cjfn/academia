<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
Route::get('/home', 'HomeController@index');

Route::resource('alumnos','alumnoController');
//datatables alumnos
Route::resource('adminalumnos','adminalumnosContipos_pagostroller');
Route::post('alumnos/delete', 'alumnoController@delete');
Route::get('alumnos/view', 'alumnoController@view');
Route::get('findAlumnosNombres', 'alumnoController@findAlumnosNombres');
Route::get('findAlumnosCursos', 'alumnoController@findAlumnosCursos');


//Route::get('alumnos/search','alumnoController@search'); -- no funciona js

Route::resource('cursos_alumnos','cursos_alumnosController');

//javascript
//Route::resource('tipos_pagos','alumnos_cursosController');
//Route::resource('cursos_de_alumnos', 'cursos_de_alumnosController');

Route::get('cursos_de_alumnos/view', 'cursos_de_alumnosController@view');

Route::get('alumnos_cursos', 'alumnos_cursosController@index');
Route::post('alumnos_cursos', 'alumnos_cursosController@add');
Route::post('alumnos_cursos/create', 'alumnos_cursosController@create');
Route::get('alumnos_cursos/view', 'alumnos_cursosController@view');
Route::post('alumnos_cursos/update', 'alumnos_cursosController@update');
Route::post('alumnos_cursos/delete', 'alumnos_cursosController@delete');



Route::get('cursos', 'cursosController@index');
Route::post('cursos', 'cursosController@add');
Route::get('cursos/view', 'cursosController@view');
Route::post('cursos/update', 'cursosController@update');
Route::post('cursos/delete', 'cursosController@delete');


Route::get('usuario', 'usuariosController@index');
Route::post('usuario', 'usuariosController@add');
Route::get('usuario/view', 'usuariosController@view');
Route::post('usuario/update', 'usuariosController@update');
Route::post('usuario/delete', 'usuariosController@delete');

Route::resource('pagos_usuarios', 'usuarios_pagosController');
Route::get('pagos', 'pagosController@index');
Route::post('pagos', 'pagosController@add');
Route::get('pagos/view', 'pagosController@view');
Route::get('alumnos/autocomplete', 'alumnoController@bscautomplete');
//select 2
Route::get("autocomplete",array('as'=>'autocomplete','uses'=> 'pagosController@autocomplete'));
Route::get('select2-autocomplete-ajax', 'alumnoController@dataAjax');
//on select change
Route::post("select-ajax", array('as'=>'select-ajax','uses'=>'pagosController@selectAjax'));

Route::resource('pago/detalles', 'view_detalle_pagosController');
Route::get('pago/detalles/view', 'view_detalle_pagosController@view');

Route::resource('pago_alumnos', 'alumno_pagosController');
//reporte todos los alumnos activos
Route::get('pdf', 'PdfController@invoice');
//reporte de constancia de pago
Route::get('constanciapago', 'pagosController@constanciapago');
//reporte de todos los alumnos de una jornada
Route::get('alumnos_jornadas', 'alumno_jornadasController@alumnos_jornadas');



Route::get('tipos_pagos', 'tipo_pagoController@index');
Route::post('tipos_pagos', 'tipo_pagoController@add');
Route::post('tipos_pagos/create', 'tipo_pagoController@create');
Route::get('tipos_pagos/view', 'tipo_pagoController@view');
Route::put('tipos_pagos/update', 'tipo_pagoController@update');
Route::put('tipos_pagos/delete', 'tipo_pagoController@delete');


Route::get('establecimiento', 'establecimientoController@index');
Route::post('establecimiento', 'establecimientoController@add');
Route::get('establecimiento/view', 'establecimientoController@view');
Route::post('establecimiento/update', 'establecimientoController@update');
Route::post('establecimiento/delete', 'establecimientoController@delete');
// cursos_tipos_pagos
Route::get('cursos_tipos_pagos', 'cursos_tipos_pagosController@index');
Route::post('cursos_tipos_pagos', 'cursos_tipos_pagosController@add');
Route::get('cursos_tipos_pagos/view', 'cursos_tipos_pagosController@view');
Route::post('cursos_tipos_pagos/update', 'cursos_tipos_pagosController@update');
Route::post('cursos_tipos_pagos/delete', 'cursos_tipos_pagosController@delete');

Route::get('salon', 'SalonController@index');
Route::post('salon', 'SalonController@add');
Route::get('salon/view', 'SalonController@view');
Route::post('salon/update', 'SalonController@update');
Route::post('salon/delete', 'SalonController@delete');


Route::post('addcursosAlumnos', 'alumnoCursoController@add');
});


Route::resource('rptconstancia','apiconstaciaPagoController', ['only'=>['index','store','show','update','destroy']]);



Route::auth();