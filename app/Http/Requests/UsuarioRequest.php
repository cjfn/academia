<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuarioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
               'name' => 'required|max:60',

            'email' => 'required|max:60',

            'rol' => 'required',

            
            

        ];
    }

    public function messages()
    {
        return[
            'email.unique' => 'Este correo ya esta siendo utilizado'
        ];
    }
}
