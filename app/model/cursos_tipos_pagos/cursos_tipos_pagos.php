<?php

namespace App\model\cursos_tipos_pagos;

use Illuminate\Database\Eloquent\Model;

class cursos_tipos_pagos extends Model
{
    //

     protected $table = 'cursos_tipos_pagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'cursos','tipo_pagos','anio','pago','observaciones','activo'
    ];
}
