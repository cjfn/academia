<?php

namespace App\model\tipo_pagos;

use Illuminate\Database\Eloquent\Model;

class tipo_pagos extends Model
{
      protected $table = 'tipo_pagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'id',
        'nombre', 
        'tipo_pago', 
        'observaciones'
    ];  
}