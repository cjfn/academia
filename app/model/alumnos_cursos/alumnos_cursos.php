<?php

namespace App\model\alumnos_cursos;

use Illuminate\Database\Eloquent\Model;

class alumnos_cursos extends Model
{
     protected $table = 'alumno_cursos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'alumno', 'curso', 'fecha_create','fecha_update','hora_inicio', 'hora_fin', 'jornada'
    ];
}
