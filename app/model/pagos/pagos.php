<?php

namespace App\model\pagos;

use Illuminate\Database\Eloquent\Model;

class pagos extends Model
{
     protected $table = 'alumno_pagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'alumno', 'cursos_tipos_pagos', 'total', 'forma_pago','nombre', 'direccion'
    ];
}
