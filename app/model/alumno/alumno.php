<?php

namespace App\model\alumno;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{

	 protected $table = 'alumno';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
         'codigo', 'nombre', 'apellido', 'departamento','municipio','establecimiento','telefono','direccion','fecha_nacimiento','ciclo','activo', 'grado'
    ];

}
