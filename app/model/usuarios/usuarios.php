<?php

namespace App\model\usuarios;

use Illuminate\Database\Eloquent\Model;

class usuarios extends Model
{
     protected $table = 'users';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'name', 'email', 'password', 'tipo_usuario'
    ];
}
