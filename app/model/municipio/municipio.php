<?php

namespace App\model\municipio;

use Illuminate\Database\Eloquent\Model;

class municipio extends Model
{
    protected $table = 'municipio';
    
    protected $fillable = [
        'nombre_municipio'
    ];
}
