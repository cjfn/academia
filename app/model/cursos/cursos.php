<?php

namespace App\model\cursos;

use Illuminate\Database\Eloquent\Model;

class cursos extends Model
{
     protected $table = 'cursos';
	 protected $primarykey='id_curso';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'descripcion','fecha_create','curso'
    ];
}
