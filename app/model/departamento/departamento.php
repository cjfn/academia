<?php

namespace App\model\departamento;

use Illuminate\Database\Eloquent\Model;

class departamento extends Model
{
   		protected $table = 'departamento';
    
    protected $fillable = [
        'nombre_departamento'
    ];
}
