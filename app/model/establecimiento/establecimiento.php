<?php

namespace App\model\establecimiento;

use Illuminate\Database\Eloquent\Model;

class establecimiento extends Model
{
    protected $table = 'establecimiento';
     protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'direccion'
    ];
}
