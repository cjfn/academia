<?php

namespace App\model\tipo_pago;

use Illuminate\Database\Eloquent\Model;

class tipo_pago extends Model
{
    protected $table = 'tipo_pagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'tipo_de_pago', 'pago', 'observaciones', 'activo'
    ];
}
