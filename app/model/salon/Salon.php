<?php

namespace App\model\salon;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
     protected $table = 'salon';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'descripcion', 'limite_alumnos'
    ];
}
