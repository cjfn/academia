<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
      protected $table = 'roles';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'name'
    ];
}
