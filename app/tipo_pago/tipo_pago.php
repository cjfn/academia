<?php

namespace App\tipo_pago;

use Illuminate\Database\Eloquent\Model;

class tipo_pago extends Model
{
    //
 	 protected $table = 'tipo_pagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'id',
        'nombre', 
        'pago', 
        'tipo_pago', 
        'observaciones'
    ];   
}
